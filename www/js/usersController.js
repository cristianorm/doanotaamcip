/**
 * Created by cristiano on 23/07/16.
 */
angular.module('UsersCtrl', [])

.controller('UsersCtrl', function($rootScope, $scope, $state, $http, $ionicPopup, $ionicLoading, $log, IntegrationData, ApiEndpoint) {

  $log.debug('Entrou no controlador UsersCtrl.');
  $log.debug('Controlador Signup/ForgetPassword');

  var origem = IntegrationData.getOrigin();
  $log.debug("Carregou origem: " + origem);

  setLayout();

  $scope.$on('$ionicView.beforeEnter', function(e) {
    //Atualiza a estrutura de layout da tela.
    setLayout();
  });

  function setLayout() {
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.isExpanded = true;
    $scope.$parent.setHeaderFab('left');
  }

  //Funcao que valida se o email esta em formato correto
  function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
  };


  //funcao para emitir alertas Ionic
  function alert(title, message) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: message,
      cssClass: 'popupDoaNota-buttons'
    });

    alertPopup.then(function (res) {
      $log.debug('Click alert, titulo: ' + title + ', Mensagem: ' + message);
    });
  };

  /*===============================
   Metodo de Criação de usuario
   ================================*/
  //Cria o objeto que suporta o model
  $scope.signup = {
    nome: '',
    email: '',
    senha: ''
  };

  $scope.doCreateUser = function () {
    //Armazena o email e senha

    var nome =  $scope.signup.nome;
    var email = $scope.signup.email;
    var senha = $scope.signup.senha;

    //Atribui os valores do formulario
    $log.debug("Signup, nome informado: " + nome);
    $log.debug("Signup, email informado: " + email);
    $log.debug("Signup, senha informada: " + senha);

    var validData = 0;

    if (nome == "") {

      alert('Nome', 'Informe seu Nome.');
      validData = 0;

    } else if (email == "") {

      alert('Email', 'Informe seu Email.');
      validData = 0;

    } else if (senha == "") {

      alert('Senha', 'Informe sua Senha.');
      validData = 0;

    } else {
      //Validacao do formato do email
      if (!validateEmail(email)) {

        alert('Email', 'Informe seu email no formato exemplo@exemplo.com.');

        validData = 0;
      } else {
        validData = 1;
      };
    };

    $log.debug("validData " + validData);

    //Armazena a URL
    var url = ApiEndpoint.url + "/createloginmobile.php?JSON_CALLBACK=?";

    //Carrega os parametros
    var config = {
      params: {
        usuario: email,
        password: senha,
        nome: nome,
        origem: origem
      }
    };

    if (validData == 1) {
      $ionicLoading.show({template: 'Criando Usuário...'});

      //Chama a URL para login no DoaNota
      $http.get(url, config).then(

        //Sucesso
        function(response) {
          $log.debug("Resposta com sucesso do DoaNota: " + response.data);

          var jsonResponse = $scope.$eval(response.data);//angular.fromJson(jsonData[1]);
          $log.debug("Conteudo extraido de Resposta: " + jsonResponse.resposta);

          $ionicLoading.hide();

          //Valida a resposta do servidor
          if (jsonResponse.validacion == "existente") {

            //Mensagem de envio de email
            alert("Usuário Existente", "Usuário já existente. Faça seu Login.");

            //Carrega o email para preencher a tela de login
            $rootScope.loginMail = email;


            $log.debug("o valor de $scope.login.email " + $scope.login.email);

            $state.go('app.login');

          } else if (jsonResponse.validacion == "incluido") {

            //Mensagem de envio de email
            alert("Usuário Criado", "Verifique seu email e ative sua conta.");
            $state.go('app.login');

          } else if (jsonResponse.validacion == "ok"){

            gravaLogin();
            $state.go('app.login');

          } else {

            alert("Criação de Usuário", "Usuário ou senha inválidos.");

          };//if (jsonResponse.resposta == "1") {
        },//function(response), Sucesso

        //Error
        function(response) {
          $ionicLoading.hide();
          $log.error("Erro na resposta do Doa Nota: " + response.data);
          alert('Login', 'Ocorreu um erro ao Criar o Usuário/Senha no DoaNota.org. Tente novamente mais tarde.');
        }//function(response), Error
      );//$http.get(url, config).then
    };//if (validData = 1) {
  };//$scope.doCreateUser = function () {

  $scope.forget = {
    email: ''
  };
  /*===============================
   Metodo de Esquecimento da senha
   ================================*/
  $scope.doResetPwd = function() {
    //Recupera o valor do campo de email

    var email = $scope.forget.email;

    $log.debug("Email digitado é para o " + email);

    var validData = 0;

    if (email == "") {

      alert('Email', 'Informe um email.');
      validData = 0;

    } else {
      //Validacao do formato do email
      if (!validateEmail(email)) {
        alert('Email', 'Informe o email no formato exemplo@exemplo.com.');

        validData = 0;
      } else {
        validData = 1;
      }
      ;
    };

    $log.debug("validData " + validData);

    //Armazena a URL
    var url = ApiEndpoint.url + "/json.php?JSON_CALLBACK=?";

    //Carrega os parametros
    var config = {
      params: {
        email: email,
        hash: "doanota",
        tipo: "recuperasenha",
        origem: origem
      }
    };

    if (validData == 1) {
      $ionicLoading.show({template: 'Carregando...'});
      //Chama a URL para login no DoaNota
      $http.get(url, config).then(

        //Sucesso
        function(response) {
          $log.debug("resposta Nota sucesso: " + response.data);

          var jsonResponse = $scope.$eval(response.data);//angular.fromJson(jsonData[1]);
          $log.debug("Conteudo extraido de Resposta: " + jsonResponse.resposta);

          $ionicLoading.hide();

          //Valida a resposta do servidor
          if (jsonResponse.resposta == "1") {

            //Mensagem de envio de email
            alert("Recuperacao de Senha", "A senha foi enviada para o email " + email + ".");
            $state.go('app.login');

          } else {

            alert("Recuperacao de Senha", "Email não encontrado.");

          };//if (jsonResponse.resposta == "1") {
        },//function(response), Sucesso
        //Error
        function(response) {
          $ionicLoading.hide();
          $log.error("Erro na resposta do Doa Nota: " + response.data);
          alert('Login', 'Ocorreu um erro ao Recuperar a senha no DoaNota.org. Tente novamente mais tarde.');
        }//function(response), Error
      );//$http.get(url, config).then

    };//if (validData = 1) {
  };//$scope.doResetPwd = function() {



});
