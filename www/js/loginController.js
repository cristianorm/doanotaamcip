/* Created by cristiano on 20/07/16.
 */

angular.module('LoginCtrl', [])

.controller('LoginCtrl', function($rootScope, $scope, $window, $timeout, $stateParams, ionicMaterialInk, $state, $q, UserService, $ionicLoading, $ionicSideMenuDelegate, $ionicHistory, $ionicPopup, $http, DatabaseService, CuponsService, $log, $cordovaAppAvailability, ConnectivityMonitor, IntegrationData, LoginFactory, ApiEndpoint) {

  $log.debug('Entrou no controller do Login.');

  var origem = IntegrationData.getOrigin();
  $log.debug("Carregou origem: " + origem);

  //Inicializa o objeto do model
  $scope.login = {
    email: "",
    senha: ""
  };

  setLayout();

  //Inicializa o banco de dados
  DatabaseService.initialize();

  //Inicializa os cards
  LoginFactory.callCardsInit(0, "init");

  var scheme;

  if (ionic.Platform.isAndroid()) {
    scheme = "com.facebook.katana";
  } else {
    scheme = "fb://";
  }

  //Verificar se ha facebook instalado
  /*appAvailability.check(
    scheme
    ,function() {
        $log.debug("Facebook instalado. Ok.");
        $scope.fbIsAvailable = true;
      },
      function () {
        $log.debug("Facebook não instalado. Não permitir login");
        $scope.fbIsAvailable = false;
      })

  //Verifica o google plus

  if (ionic.Platform.isAndroid()) {
    scheme = "com.google.android.apps.plus";
  } else {
    scheme = "gplus://";
  }

  appAvailability.check(
    scheme
    ,function() {
      $log.debug("Plus instalado. Ok.");
      $scope.googleIsAvailable = true;
    },
    function () {
      $log.debug("Google plus não instalado. Não permitir login");
      $scope.googleIsAvailable = false;
    })*/

  /*try {
    $window.plugins.googleplus.isAvailable(
      function (available) {
        if (available) {

          $log.debug("Aplicativo Google+ disponível.");
          $scope.googleIsAvailable = available;
          // show the Google+ sign-in button
        } else {
          $log.debug("Aplicativo Google+ não disponível.");
          $scope.googleIsAvailable = available;
        }
      }
    );

  } catch (err) {
    $scope.googleIsAvailable = false;
    $log.error("Erro ao validar google plus. " + $scope.googleIsAvailable);
  }*/

  $scope.$on('$ionicView.beforeEnter', function(e) {
    //Atualiza a estrutura de layout do login.
    setLayout();
  });

  function setLayout() {
    $scope.$parent.clearFabs();

    $timeout(function() {
      //$scope.$parent.hideHeader();
      //Desativa slide do menu
      $scope.$root.showMenuIcon = false;
      $ionicSideMenuDelegate.canDragContent(false);
    }, 0);

    //ionicMaterialInk.displayEffect();
  }

  //Verifica se ha valor no controlador e atribui ao campo
  if ($rootScope.loginMail != "") {
    $scope.login.email = $rootScope.loginMail;
  };

  //Funcao que valida se o email esta em formato correto
  function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
  };

  //Funcao que grava o login do usuario no Web LocalStorage
  function gravaLogin(authReponse, userID, name, email, pictureUrl, loginMethod, logado) {

    UserService.setUser({
      authResponse: authReponse,
      userID: userID,
      name: name,
      email: email,
      picture : pictureUrl,
      loginMethod: loginMethod,
      logado: logado,
      multiplos: 1,
      cameraIniciar: 0,
      logEnabled: 0
    });

    //Inicializa o banco de dados
    //DatabaseService.initialize();

    //Armazena zero na quantidade de cupons local
    CuponsService.setQtdCupons(0);

  }

  //funcao para emitir alertas Ionic
  function alert(title, message) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: message,
      cssClass: 'popupDoaNota-buttons'
    });

    alertPopup.then(function (res) {
      $log.debug('Click alert, titulo: ' + title + ', Mensagem: ' + message);
    });
  };

  function checkConnection () {
    //
    $log.debug("Status da conexao é: " + ConnectivityMonitor.isOffline());

    //Verifica se o usuario está com internet.
    if (ConnectivityMonitor.isOffline()) {

      var alertPopup = $ionicPopup.alert({
        title: "Você está sem Internet.",
        template: "Sem conexão, não é possível realizar login. ",
        cssClass: 'popupDoaNota-buttons'
      });
      return false;
    } else {
      return true;
    }
  }

  $scope.doLogin = function () {
    $log.debug("Recupera o usuario atual");
    $log.debug(UserService.getUser().name);
    $log.debug("Metodo: " +  UserService.getUser().loginMethod);
    $log.debug("UserId: " +  UserService.getUser().userID);

    //Armazena o email e senha
    var email = $scope.login.email;
    var senha = $scope.login.senha;

    //Atribui os valores do formulario
    $log.debug("Login, usuario: " + email);
    $log.debug("Login, Senha: " + senha);

    var validData = 0;

    //Verifica se ha conexão.
    if (!checkConnection()) {
      return;
    }

    if (email == "" || email == undefined) {

      alert('Email', 'Informe seu email.');
      validData = 0;
      return;

    } else if (senha == "") {

      alert('Senha', 'Informe sua senha.');
      validData = 0;
      return;

    } else {
      //Validacao do formato do email
      if (!validateEmail(email)) {

        alert('Email', 'Informe seu email no formato exemplo@exemplo.com.');
        validData = 0;
        return;

      } else {
        validData = 1;
      };
    };

    $log.debug("validData " + validData + ", email: " + email + ", senha: " + senha);

    //Armazena a URL
    var url = ApiEndpoint.url + "/validaloginmobile.php?JSON_CALLBACK=?";

    //Carrega os parametros
    var config = {
      params: {usuario: email
        ,password: senha
        ,origem: origem}
    };

    if (validData == 1) {
      $ionicLoading.show(
        {
          template: 'Carregando...'
        , duration: 10000
        }
      );
      //Chama a URL para login no DoaNota
      $http.get(url, config).then(

        //Sucesso
        function(response) {
          $log.debug("resposta DoaNota sucesso: " + response.data);

          //Le o conteudo do parenteses da resposta
          //regExp = /\(([^)]+)\)/;
          //var jsonData = regExp.exec(response.data);
          var jsonResponse = $scope.$eval(response.data); //angular.fromJson(jsonData[1]);
          $log.debug("Conteudo extraido de Validacion: " + jsonResponse.validacion);

          $ionicLoading.hide();

          //Valida a resposta do servidor
          if (jsonResponse.validacion == "ok") {

            //Persiste login
            gravaLogin(jsonResponse.message, email, jsonResponse.nome, email, 'img/defaultAvatar.png',"DoaNota.org", true);

            $ionicHistory.nextViewOptions({
              disableBack: true
            });

            $state.go('app.doaNota');

          } else if (jsonResponse.validacion == "inativo") {

            alert("Login", "Usuário Inativo. Ative seu usuário para ter acesso.");

          } else {

            alert("Login", "Erro de validação: Usuário ou senha inválidos.");

          };//if (jsonResponse.validacion == "ok") {
        },//function(response), Sucesso
        //Error
        function(response) {
          $ionicLoading.hide();
          $log.error("Erro na resposta do Doa Nota: " + response.validacion);
          alert('Login', 'Ocorreu um erro ao efetuar login no DoaNota.org. Tente novamente mais tarde.');
        }//function(response), Error
      );//$http.get(url, config).then

    };//if (validData = 1) {
  }//$scope.doLogin = function () {


  /*===========================
   Metodo de Login do Facebook
   ===========================*/
  // This is the success callback from the login method
  var fbLoginSuccess = function(response) {

    $log.debug("Rodou o signin, entrou na funcao de sucesso fbLoginSuccess.")

    if (!response.authResponse){
      fbLoginError("Cannot find the authResponse");
      return;
    }

    var authResponse = response.authResponse;

    getFacebookProfileInfo(authResponse)
      .then(function(profileInfo) {

        $log.debug("Pegou a autorização, grava o login");

        gravaLogin(
          authResponse,
          profileInfo.id,
          profileInfo.name,
          profileInfo.email,
          "http://graph.facebook.com/" + authResponse.userID + "/picture?type=large",
          "Facebook",
          true);

        $ionicLoading.hide();
        $state.go('app.doaNota');
        //$rootScope.doRefresh();
      }, function(fail){

        $log.debug("Falha ao pegar a autorização. ");
        // Fail get profile info
        $ionicLoading.hide();
        $log.error('profile info fail', fail);
      });
  }; //fbLoginSuccess = function

  // This is the fail callback from the login method
  var fbLoginError = function(error){
    $log.error('fbLoginError', error);
    $ionicLoading.hide();
  };

  // This method is to get the user profile info from the facebook api
  var getFacebookProfileInfo = function (authResponse) {
    var info = $q.defer();

    facebookConnectPlugin.api('/me?fields=email,name&access_token=' + authResponse.accessToken, null,
      function (response) {
        $log.debug(response);
        info.resolve(response);
      },
      function (response) {
        $log.error(response);
        info.reject(response);
      }
    );
    return info.promise;
  };//getFacebookProfileInfo = function

  //This method is executed when the user press the "Login with facebook" button
  $scope.facebookSignIn = function() {

    //Verifica se ha conexão.
    if (!checkConnection()) {
      return;
    }

    $ionicLoading.show({
      template: 'Carregando...'
      , duration: 10000
    });

    facebookConnectPlugin.getLoginStatus(function(success){

      $log.debug('Entrou processo de login, status é ' + success.status);
      if(success.status === 'connected'){
        // The user is logged in and has authenticated your app, and response.authResponse supplies
        // the user's ID, a valid access token, a signed request, and the time the access token
        // and signed request each expire
        $log.debug('Usuario Logado é : ' + UserService.getUser().name);
        $log.debug('getLoginStatus', success.status);


        // Check if we have our user saved
        var user = UserService.getUser().userID;
        $log.debug("Avaliou user, expressao é " + !user + " e valor é: " + user);

        if(!user){
          getFacebookProfileInfo(success.authResponse)
            .then(function(profileInfo) {

              $log.debug("Get profile info, entrou na funcao de sucesso, email é " + profileInfo.email);

              //Grava as informacoes de login do facebook
              gravaLogin(
                success.authResponse,
                profileInfo.id,
                profileInfo.name,
                profileInfo.email,
                "http://graph.facebook.com/" + success.authResponse.userID + "/picture?type=large",
                "Facebook"
                , true);

              /*UserService.setUser({
                authResponse: success.authResponse,
                userID: profileInfo.id,
                name: profileInfo.name,
                email: profileInfo.email,
                picture : "http://graph.facebook.com/" + success.authResponse.userID + "/picture?type=large",
                loginMethod: "facebook"
              });*/
              $ionicLoading.hide();
              $state.go('app.doaNota');
              $rootScope.doRefresh();
            }, function(fail){

              $log.debug("Get profile info, entrou na funcao de erro, fail message " + fail);

              // Fail get profile info
              $ionicLoading.hide();
              $log.error('profile info fail', fail);
            });
        }else{

          $log.debug("Avaliou user, expressao é " +!User + " e valor é " + user);

          $ionicLoading.hide();
          $state.go('app.doaNota');
          $rootScope.doRefresh();
        }
      } else {
        // If (success.status === 'not_authorized') the user is logged in to Facebook,
        // but has not authenticated your app
        // Else the person is not logged into Facebook,
        // so we're not sure if they are logged into this app or not.

        $log.debug('Else, getLoginStatus é ', success.status);

        $ionicLoading.show({
          template: 'Carregando...'
          , duration: 10000
        });

        // Ask the permissions you need. You can learn more about
        // FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
        facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
      }
    });

    $scope.user = UserService.getUser();

    //Desativa a navegacao da proxima tela.
    $ionicHistory.nextViewOptions({
      disableBack: true
    });

  };//$scope.facebookSignIn = function()
  /*===========================
   Metodo de Login do Facebook FIM
   ===========================*/


  /*===========================
   Metodo de Login do Google INICIO
   ===========================*/

  // This method is executed when the user press the "Sign in with Google" button
  $scope.googleSignIn = function() {

    //Verifica se ha conexão.
    if (!checkConnection()) {
      return;
    }

    $ionicLoading.show({
        template: 'Carregando...'
      , duration: 10000
    });

    $window.plugins.googleplus.login(
      {},
      function (profileInfo) {
        // For the purpose of this example I will store user data on local storage

        //Grava as informacoes de login do google
        gravaLogin(
          profileInfo.accessToken,
          profileInfo.id,
          profileInfo.displayName,
          profileInfo.email,
          profileInfo.imageUrl,
          "Google"
          , true);

        $log.debug("Dados do login do google: Nome " + UserService.getUser().name);
        $log.debug("Dados do login do google: Imagem " + UserService.getUser().picture);

        $ionicLoading.hide();
        $state.go('app.doaNota');
        $rootScope.doRefresh();
      },
      function (msg) {
        alert("Login Google", "Erro no Login Google. Tente novamente mais tarde ou outra forma de login.")
        $log.debug(msg);
        $ionicLoading.hide();
      }
    );

    $scope.user = UserService.getUser();

    //Desativa a navegacao da proxima tela.
    $ionicHistory.nextViewOptions({
      disableBack: true
    });
  };

  /*===========================
   Metodo de Login do Google FIM
   ===========================*/
})
