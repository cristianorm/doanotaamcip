angular.module('doaNota.controllers', [])

.controller('AppCtrl', function($rootScope, $scope, $window, $ionicModal, $ionicPlatform, $timeout, $cordovaSocialSharing, UserService, $ionicActionSheet, $state, $ionicLoading, $ionicPopup, $ionicHistory, CuponsService, $log, $cordovaVibration, IntegrationData) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
    $scope.$on('$ionicView.enter', function(e) {
    //Carrega as variaveis de logon
    $scope.name = UserService.getUser().name;
    $scope.loginMethod = UserService.getUser().loginMethod;
    $scope.picture = UserService.getUser().picture;

    $rootScope.picture = $scope.picture;

    });

  $ionicPlatform.onHardwareBackButton(function(e) {
    event.preventDefault();
    event.stopPropagation();

    var confirmDonation = $ionicPopup.confirm({
      title: "Sair",
      template: 'Deseja sair do Doa Nota?',
      cssClass: 'popupDoaNota-buttons',
      cancelText: 'Cancelar',
      okText: 'Sair'
    });

    confirmDonation.then(function(res) {
      if(res) {
        $log.debug('Confirmado Saida.');
        navigator.app.exitApp();
      } else {
        $log.debug('Nao confirmado a Saida. .');
        return;
      }
    });

  });

  $log.debug("Controlador Principal/Menu");

  $log.debug("Multiplos: " + UserService.getUser().multiplos);
  $log.debug("CameraIniciar: " + UserService.getUser().cameraIniciar);

  ////////////////////////////////////////
  // Layout Methods
  ////////////////////////////////////////
  $scope.hideNavBar = function() {
    document.getElementsByTagName('ion-nav-bar')[0].style.display = 'none';
  };

  $scope.showNavBar = function() {
    document.getElementsByTagName('ion-nav-bar')[0].style.display = 'block';
  };

  $scope.noHeader = function() {
    var content = document.getElementsByTagName('ion-content');
    for (var i = 0; i < content.length; i++) {
      if (content[i].classList.contains('has-header')) {
        content[i].classList.toggle('has-header');
      }
    }
  };

  $scope.setExpanded = function(bool) {
    $scope.isExpanded = bool;
  };

  $scope.setHeaderFab = function(location) {
    var hasHeaderFabLeft = false;
    var hasHeaderFabRight = false;

    switch (location) {
      case 'left':
        hasHeaderFabLeft = true;
        break;
      case 'right':
        hasHeaderFabRight = true;
        break;
    }

    $scope.hasHeaderFabLeft = hasHeaderFabLeft;
    $scope.hasHeaderFabRight = hasHeaderFabRight;
  };

  $scope.hasHeader = function() {
    var content = document.getElementsByTagName('ion-content');
    for (var i = 0; i < content.length; i++) {
      if (!content[i].classList.contains('has-header')) {
        content[i].classList.toggle('has-header');
      }
    }

  };

  $scope.hideHeader = function() {
    $scope.hideNavBar();
    $scope.noHeader();
  };

  $scope.showHeader = function() {
    $scope.showNavBar();
    $scope.hasHeader();
  };

  $scope.clearFabs = function() {
    var fabs = document.getElementsByClassName('button-fab');
    if (fabs.length && fabs.length > 1) {
      fabs[0].remove();
    }
  };

  //Verifica se deve ir direto à pagina do main, caso esteja logado.
  $scope.logado        = UserService.getUser().logado;
  var name             = UserService.getUser().name;
  var varCameraIniciar = UserService.getUser().cameraIniciar;

  $log.debug("Valor de logado: " + String($scope.logado));
  $log.debug("Valor do Nome logado: " + name);

  //Define a quantidade de cupons do Servico
  CuponsService.setQtdCupons(CuponsService.getQtdCupons());

  /*if ($scope.logado != true) {
      $state.go("app.login");
  } else {
    //Verifica se deve abrir a camera ao iniciar
    if (varCameraIniciar == 1) {
      $rootScope.$emit("abreCamera");
    }
    //Define a quantida de cupons do Servico
    CuponsService.setQtdCupons(CuponsService.getQtdCupons());
  };*/

  //});
  // Form data for the login modal
  $scope.loginData = {};
  $scope.isExpanded = false;
  $scope.hasHeaderFabLeft = false;
  $scope.hasHeaderFabRight = false;

  var navIcons = document.getElementsByClassName('ion-navicon');
  for (var i = 0; i < navIcons.length; i++) {
    navIcons.addEventListener('click', function() {
      this.classList.toggle('active');
    });
  }

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    $log.debug('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };


  ////////////////////////////////////////
  // Methods de Compartilhamento
  ////////////////////////////////////////
  var shareUrl;

  //Armazena a URL de Acordo com a plataforma
  if (ionic.Platform.isAndroid()) {
    shareUrl = IntegrationData.getAndroidShareUrl();
    $log.debug("É android, url de share é " + shareUrl);
  } else {
    shareUrl = IntegrationData.getAppleShareUrl();
    $log.debug("É IOS, url de share é " + shareUrl);
  }

  // this is the complete list of currently supported params you can pass to the plugin (all optional)
  var options = {
    //Essa mensagem e importante apenas para email.
    message: 'Doe sua nota fiscal hoje com o Aplicativo ' + IntegrationData.getAppName() + '.', // not supported on some apps (Facebook, Instagram)
    subject: 'Doe sua nota fiscal hoje com o Aplicativo ' + IntegrationData.getAppName() + '.',

    //Para Doar via Instagram, apenas com image.
    //files: ["www/DoaNotaLogin.jpg"], // an array of filenames either locally or remotely
    url: shareUrl,
    chooserTitle: 'Ajude a ' + IntegrationData.getEntity() // Android only, you can override the default share sheet title
  }

  var onSuccess = function(result) {
    $log.debug("Compartilhamento completado? " + result.completed); // On Android apps mostly return false even while it's true
    $log.debug("Compartilhado no App: " + result.app); // On Android result.app is currently empty. On iOS it's empty when sharing is cancelled (result.completed=false)
  }

  var onError = function(msg) {
    $log.error("Compartilhamento falhou com a mensagem: " + msg);
  }

  $scope.share = function() {

    $cordovaVibration.vibrate(50);
    $window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);
    //$cordovaSocialSharing.share("Doe sua nota fiscal hoje com o Aplicativo DoaNota.", "img/DoaNotaLogin.jpg", "https://play.google.com/store/apps/details?id=org.apache.cordova.doanota");
  }

  /*==========================
        Logout do Menu
   ==========================*/
  $scope.user = UserService.getUser();

  $scope.showLogOutMenu = function () {

    var confirmPopup = $ionicPopup.confirm({
      title: 'Logout',
      template: 'Tem certeza que deseja fazer logout?',
      cssClass: 'popupDoaNota-buttons',
      cancelText: 'Cancelar',
      okText: 'OK'
    });

    confirmPopup.then(function (res) {
      if (res) {
        $log.debug('chamando a funcao de logout');
        logout();
      } else {
        $log.debug('Chamando a funcao de cancelar');
        cancel();
      }
    });

    function cancel() {
      $log.debug('Logout Cancelado');
    };

    function logout() {

      //Desativa a navegacao da proxima tela.
      $ionicHistory.nextViewOptions({
        disableBack: true
      });

      $ionicLoading.show({
        template: 'Logging out...'
      });

      //Verifica o metodo que está logado

      if (UserService.getUser().loginMethod == "facebook") {

        $log.debug("Efetuando logout do facebook");

        // Facebook logout
        facebookConnectPlugin.logout(function () {
            $ionicLoading.hide();

            //Deleta o usuario do localStorage
            UserService.delUser(UserService.getUser().userID);

            $state.go('app.login');

          },
          function (fail) {

            $log.error("Falha ao efetuar o logout do facebook: " + fail);
            $ionicLoading.hide();
          });

      }  else if (UserService.getUser().loginMethod == "facebook") {

        $log.debug("UserId é " + UserService.getUser().userID);
        $log.debug("UserName e Login method é " + UserService.getUser().name + ", " + UserService.getUser().loginMethod);

        // Google logout
        window.plugins.googleplus.logout(
          function (msg) {
            $log.debug("Efetuando logout google, msg: " + msg);
            $ionicLoading.hide();

            UserService.delUser(UserService.getUser().userID);

            $state.go('app.login');

          },
          function(fail){
            $log.error("Erro efetuando logout google, msg: " + fail);
            $ionicLoading.hide();
          }
        );

      } else {

        $log.debug("Efetuando logout generico.")
        $log.debug("UserId é " + UserService.getUser().userID);
        $log.debug("UserName e Login method é " + UserService.getUser().name + ", " + UserService.getUser().loginMethod);

        $ionicLoading.hide();

        UserService.delUser(UserService.getUser().userID);

        $state.go('app.login');
      };

    }
  }
    /*function() {
    var hideSheet = $ionicActionSheet.show({
      destructiveText: 'Logout',
      titleText: 'Are you sure you want to logout? This app is awsome so I recommend you to stay.',
      cancelText: 'Cancel',
      cancel: function() {},
      buttonClicked: function(index) {
        return true;
      },
      destructiveButtonClicked: function(){
        $ionicLoading.show({
          template: 'Logging out...'
        });


        // Facebook logout
        facebookConnectPlugin.logout(function(){
            $ionicLoading.hide();
            $state.go('app.login');
          },
          function(fail){
            $ionicLoading.hide();
          });
      }
    });
  };*/
}) //Fim AppCtrl

.controller('ConfigCtrl', function($rootScope, $scope, $http, $stateParams, $ionicPopup, UserService, $ionicLoading, $log, IntegrationData, ApiEndpoint) {

  $log.debug('Controlador ConfigCtrl');

  //funcao para emitir alertas Ionic
  function alert(title, message) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: message,
      cssClass: 'popupDoaNota-buttons'
    });

    alertPopup.then(function (res) {
      $log.debug('Click alert, titulo: ' + title + ', Mensagem: ' + message);
    });
  };

  reloadParameters();

  $scope.$on('$ionicView.beforeEnter', function(e) {
    //Atualiza os controles de acordo com o servico localStorage.
    reloadParameters();

  });

  // Set Header
  $scope.$parent.showHeader();
  $scope.$parent.clearFabs();
  $scope.isExpanded = false;
  $scope.$parent.setExpanded(false);
  $scope.$parent.setHeaderFab(false);

  $scope.$parent.showHeader();
  $scope.$parent.clearFabs();
  $scope.isExpanded = true;
  $scope.$parent.setHeaderFab('left');

  //Inicializa o objeto do view
  function reloadParameters() {
    $scope.config = {};

    $scope.config.multiplos = UserService.getUser().multiplos;
    $scope.config.cameraIniciar = UserService.getUser().cameraIniciar;

  }

  $scope.picture = $rootScope.picture;

  $scope.configChange = function () {

    //Grava os dados do usuario ao modificar os toggles
    var multiplos     = $scope.config.multiplos;
    var cameraIniciar = $scope.config.cameraIniciar;

    if (multiplos == undefined) {
      multiplos = 0;
    } else if(cameraIniciar == undefined) {
      cameraIniciar = 0;
    }

    var userData = UserService.getUser();

    userData.multiplos = multiplos;
    userData.cameraIniciar = cameraIniciar;

    UserService.setUser(userData);

  }

  $scope.doChangePassword = function () {

    //Campos de entrada
    var novaSenha = $scope.config.novaSenha;

    //Recupera o email do usuario
    var email = UserService.getUser().email;

    var validData = 0;

    if (novaSenha == "" || novaSenha == undefined) {

      alert('Nova Senha', 'Informe uma nova senha.');
      validData = 0;

    } else {
      validData = 1;
    };

    $log.debug("validData " + validData);

    //Armazena a URL
    var url = ApiEndpoint.url + "/json.php?JSON_CALLBACK=?";

    //Variaveis config
    var hash   = "doanota";
    var tipo   = "novasenha";
    var origem = IntegrationData.getOrigin();

    $log.debug("Carregou Origem: " + origem);

    //Carrega os parametros
    var config = {
      params: {
        email: email,
        novasenha: novaSenha,
        hash: hash,
        tipo: tipo
      }
    };

    if (validData == 1) {
      $ionicLoading.show({template: 'Alterando Senha...'});

      //Chama a URL para login no DoaNota
      $http.get(url, config).then(

        //Sucesso
        function(response) {
          $log.debug("Resposta com sucesso do DoaNota: " + response.data);

          var jsonResponse = $scope.$eval(response.data);//angular.fromJson(jsonData[1]);
          $log.debug("Conteudo extraido de Resposta: " + jsonResponse.resposta);

          $ionicLoading.hide();

          //Valida a resposta do servidor
          if (jsonResponse.resposta == "1") {
            alert("Nova Senha", "Senha alterada com sucesso.");
          } else {
            //Mensagem de envio de email
            alert("Nova Senha", "Erro ao alterar a Senha. Reporte por favor à bug@doanota.org.");
          };//if (jsonResponse.resposta == "1") {
        },//function(response), Sucesso

        //Error
        function(response) {
          $ionicLoading.hide();
          $log.error("Erro na resposta do Doa Nota: " + response.data);
          alert('Login', 'Ocorreu um erro ao definir a nova senha no DoaNota.org. Tente novamente mais tarde.');
        }//function(response), Error
      );//$http.get(url, config).then
    };//if (validData = 1) {
  }
})

.controller('ProfileCtrl', function($scope, $stateParams, UserService, $log) {

  $log.debug('Controlador MeuPerfil');
  $log.debug("Nome armazenado " + UserService.getUser().name);

  $scope.$on('$ionicView.beforeEnter', function(e) {

    //Atualiza os dados do perfil a cada visita a view.
    updateProfile();

  });

  updateProfile();

  function updateProfile() {

    $scope.name        = UserService.getUser().name;
    var loginMethod    = UserService.getUser().loginMethod;
    $scope.loginMethod = loginMethod;
    $scope.picture     = UserService.getUser().picture;
    $scope.email       = UserService.getUser().email;
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.isExpanded = true;
    $scope.$parent.setHeaderFab('left');
  }
})

.controller('HelpCtrl', function($rootScope, $scope, $state, UserService, $ionicLoading, $http, $ionicHistory, $log, IntegrationData, $ionicPopup, ApiEndpoint) {

  //funcao para emitir alertas Ionic
  function alert(title, message) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: message,
      cssClass: 'popupDoaNota-buttons'
    });

    alertPopup.then(function (res) {
      $log.debug('Click alert, titulo: ' + title + ', Mensagem: ' + message);
    });
  };


  var origem = IntegrationData.getOrigin();
  $scope.appName = IntegrationData.getAppName();

  $log.debug("Entrou no controlador HelpCtrl");
  $log.debug("Carregou origem: " + origem);

  $scope.picture = $rootScope.picture;

  /*$log.debug("o Valor de vbackview é " + $ionicHistory.backView());
  $log.debug("o Valor de current.name é " + $state.current.name);
  $log.debug("o Valor de $ionicHistory.viewHistory().backView " + $ionicHistory.viewHistory().backView);

  $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
    viewData.enableBack = true;

    $ionicHistory.viewHistory().backView = 'app.doaNota';

    if($ionicHistory.backView() != null) {
      $rootScope.appScope.viewBack[$state.current.name] = $ionicHistory.backView();
    }
    else if(typeof $rootScope.appScope.viewBack[$state.current.name] != undefined) {
      $ionicHistory.viewHistory().backView = $rootScope.appScope.viewBack[$state.current.name];
    }
  });*/

  $scope.groups = [];
  for (var i=0; i<5; i++) {

    if (i == 0 ) {
      $scope.groups[i] = {
        name: "Quem Somos",
        items: [],
        show: false
      };
      //for (var j=0; j<3; j++) {
      $scope.groups[i].items.push(i);


      var item0= "<div>"
        + "<p class='text-pre-wrap' align='justify'>Associação Mantenedora do Centro Integrado de Prevenção"
        + "</p></div>"
        + "<p class='text-pre-wrap' align=' justify'>" +

        "A AMCIP teve sua origem em 22/06/94 no extinto CEPEDAPE. " +
        "Sua criação está ligada a um grupo de mães e profissionais, que comungavam dos mesmos princípios " +
        "filosóficos com relação às crianças portadoras de necessidades educativas especiais que é a integração " +
        "e inclusão no ensino regular."
        + "<br>" +

        "<br/><strong>Missão</strong>" +

        "</br>Atendimento especializado de reabilitação infantil, visando desenvolvimento de potencialidades e tendo no foco: " +
        "- Independência</br>"+
        "- Socialização </br>"+
        "- Inclusão sócio-educacional</br>" +

        "<br/><strong>Visão</strong>" +

        "</br>Ser um Centro de Referência no atendimento do bebê de risco e com deficiência, através da detecção e estimulação precoce.<br/>" +

        "<br/><strong>Valores</strong>" +

        "</br>" +
        "Ética</br>"+
        "Profissionalismo</br>"+
        "Responsabilidade</br>"

    } else if (i == 1) {

      $scope.groups[i] = {
        name: "Entre em contato conosco",
        items: [],
        show: false
      };
      //for (var j=0; j<3; j++) {
      $scope.groups[i].items.push(i);

      var item1 =
        "<strong>Entre em contato conosco</strong></br>" +
        "<p class='text-pre-wrap' ><strong>Endereço: </strong>Rua Imaculada Conceição, 935 - Prado Velho - Curitiba/PR<br/>" +
        "<strong>CEP: 80.215-030</strong><br/>" +
        "<strong>Email: </strong>amcip@terra.com.br<br/>" +
        "<strong>Telefone: </strong>(41) 3333-6820<br/>" +
        "Fique à vontade para tirar suas dúvidas e questionamentos.<br/>"

    } else if (i == 2) {
      $scope.groups[i] = {
       name: "Pergunte à Nós",
       items: [],
       show: false
       };
       //for (var j=0; j<3; j++) {
       $scope.groups[i].items.push(i);

    } else if (i == 3) {

      $scope.groups[i] = {
        name: "Perguntas e Respostas",
        items: [],
        show: false
      };
      //for (var j=0; j<3; j++) {
      $scope.groups[i].items.push(i);

      var item3 =
        "<p class='text-pre-wrap' align='justify'><strong>Eu preciso digitar os dados do Cupom?<br></strong>"
        + "Não, o <strong>Doa Nota</strong> faz a leitura das notas e digitalização. Você só precisa tirar uma foto nítida.</p>"

        + "<p class='text-pre-wrap' align='justify'><strong>Qual valor da doação?<br></strong>"
        + "Não existe precisão quanto ao valor. Depende do ramo de atividade e apuração do imposto, mas, na média, cada doação feita por você, a Entidade irá receber R$ 0,50 centavos, ou seja, em um mês você pode poderá doar facilmente mais de R$ 20,00.</p>"

        + "<p class='text-pre-wrap' align='justify'><strong>Posso doar sem acesso a Internet?<br></strong>"
        + "Sim, se a doação for feita sem acesso a internet, elas serão armazenadas em seu celular, quando você se conectar em um Wifi ou um 3G, basta clicar em [Enviar Cupons Pendentes].</p>"

        + "<p class='text-pre-wrap' align='justify'><strong>Posso doar com CPF?<br></strong>"
        + "<p class='text-pre-wrap' align='justify'>As notas não poderão possuir CPF. Notas com CPF já foram direcionadas para o CPF indicado.</p>"

        + "<p class='text-pre-wrap' align='justify'><strong>Qual prazo para doação?</strong><br>"
        + "O Cupom fiscal deverá ser enviado até o dia 20 do mês subsequente à data de emissão.<br>"
        + "Exemplo: cupom fiscal emitido em julho, deverá ser cadastrado até o dia 20 de agosto. </p>";

    } else if (i == 4) {

        $scope.groups[i] = {
          name: "Tecnologia",
          items: [],
          show: false
        };
        //for (var j=0; j<3; j++) {
        $scope.groups[i].items.push(i);

        var item4 =
        "<img class='img-doaNota' src='img/doaNotaIconTransparent.png'>" +
        "<p class='text-pre-wrap'>Este aplicativo foi desenvolvido por <b>DoaNota.org</b><br>" +
        "Saiba mais em nosso <a href='http://doanota.org'>Site</a><br></p>" +
        "<p> Versão do Aplicativo " + IntegrationData.getAppVersion() + "</p>";

    } else {

      $scope.groups[i] = {
        name: i,
        items: [],
        show: false
      };
      for (var j=0; j<3; j++) {
        $scope.groups[i].items.push(i + '-' + j);
      }

    }

  }

  /*
   * if given group is the selected group, deselect it
   * else, select the given group
   */
  $scope.toggleGroup = function(group) {
    group.show = !group.show;
  };
  $scope.isGroupShown = function(group) {
    return group.show;
  };

  $scope.help = {};

  $scope.doSendQuestion = function () {

    var question = $scope.help.question;
    var email    = UserService.getUser().email;

    if (question == "" || question == undefined ) {
      alert("Pergunta", "Informe uma pergunta. ");
    } else {
      //Armazena a URL
      var url = ApiEndpoint.url + "/pergunta.php?JSON_CALLBACK=?";

      //Carrega os parametros
      var config = {
        params: {
          usuario: email,
          pergunta: question,
          origem: origem
        }
      };

      //$log.debug("Variável config Help: " + JSON.stringify(config));

      $ionicLoading.show({template: 'Enviando...'});
      //Chama a URL para login no DoaNota
      $http.get(url, config).then(

        //Sucesso
        function(response) {
          $log.debug("Sucesso na resposta do Doa Nota: " + response.data);
          alert("Obrigado", "Mensagem enviada com sucesso!");

          $ionicLoading.hide();
        },//function(response) {, Sucesso

        //Error
        function(response) {
          $ionicLoading.hide();
          $log.error("Erro na resposta do Doa Nota: " + response.data);
          alert('Erro', 'Erro ao enviar a mensagem. Tente novamente mais tarde.');
        }//function(response), Error
      )

    };
  };//$scope.doSendQuestion = function () {


  $scope.getHtml = function (item) {

    if (item == 0 ) {
      return item0;
    } else if (item == 1) {
      return item1;
    } else if (item == 2) {
      return item2;
    } else if (item == 3) {
      return item3;
    } else if (item == 4) {
      return item4;
    } else if (item == 5) {
      return item5;
    } else if (item == 6) {
      return item6;
    }
  };

})

;
