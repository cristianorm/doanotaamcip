// Ionic Starter App// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('doaNotaAmcip', ['ionic', 'doaNota.controllers', 'LoginCtrl', 'UsersCtrl', 'RankingCtrl', 'DoaNotaCtrl', 'OngCtrl', 'ngCordova', 'doaNota.services', 'ionic-material', 'ionMdInput', 'ion-floating-menu'])

.run(function($ionicPlatform, $ionicPopup, $state) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)

    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      //cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    if (window.Connection) {
      if(navigator.connection.type == Connection.NONE) {

        var alertPopup = $ionicPopup.alert({
          title: "Você está sem Internet.",
          template: "Algumas funcionalidades podem estar indisponíveis. Você pode " +
          "guardar suas notas e enviar quando voltar a ficar on-line."
          ,
          cssClass: 'popupDoaNota-buttons'
        });
      }else{
        console.log("DoaNota, app.run. Conexao detectada " + navigator.connection.type);
      }
    }else{
      //alert('Nao foi possivel identificar Window.connection.');
      console.log('Nao foi possivel identificar Window.connection.');
    }

    //Se precisar travar a orientação
    //lockedAllowed = window.screen.lockOrientation("portrait");

    /*$timeout(function() {
        $state.go('app.doaNota');
        }, 5000);*/

    //Recupera os dados do localStorage. Esta em um service mas nao pode ser injetado aqui.
    userData = JSON.parse(window.localStorage.userData || '{}');
    var logado = userData.logado;

    console.log("Doa nota, app.js, primeira pagina. Logado: " + logado);

    if (logado != true) {
      $state.go("app.login");
    } else {
      $state.go("app.doaNota");
    }

    setTimeout(function() {
      navigator.splashscreen.hide();
      }, 2000);
  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $logProvider) {

  //Turn on and off Log
  $logProvider.debugEnabled(false);

  // Turn off caching for demo simplicity's sake
  //$ionicConfigProvider.views.maxCache(0);
  $ionicConfigProvider.views.maxCache(30);
  $ionicConfigProvider.views.forwardCache(0);

  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

    .state('app.login', {
      url: '/login',
      views: {
        'menuContent': {
          templateUrl: 'templates/login.html',
          controller: 'LoginCtrl'
        },
        'fabContent': {
          template: ''
        }
      }
    })

  .state('app.forgetPassword', {
    url: '/forgetPassword',
    views: {
      'menuContent': {
        templateUrl: 'templates/forgetPassword.html',
        controller: 'UsersCtrl'
      },
      'fabContent': {
        template: ''
      }
    }
  })

  .state('app.help', {
    url: '/help',
    views: {
      'menuContent': {
        templateUrl: 'templates/help.html',
        controller: 'HelpCtrl'
      }
    }
  })

  .state('app.ranking', {
      url: '/ranking',
      views: {
        'menuContent': {
          templateUrl: 'templates/ranking.html',
          controller: 'RankingCtrl'
        }
      }
    })
    .state('app.doaNota', {
      url: '/doaNota',
      views: {
        'menuContent': {
          templateUrl: 'templates/doaNota.html',
          controller: 'DoaNotaCtrl'
        },
        'fabContent': {
          template: '<button id="fab-activity" class="button button-fab button-fab-top-right expanded button-energized-900 flap"><i class="icon ion-paper-airplane"></i></button>',
          controller: function ($timeout) {
            $timeout(function () {
              document.getElementById('fab-activity').classList.toggle('on');
            }, 200);
          }
        }

      }
    })

    .state('app.config', {
      url: '/config',
      views: {
        'menuContent': {
          templateUrl: 'templates/config.html',
          controller: 'ConfigCtrl'
        }
      }
    })

  .state('app.profile', {
    url: '/profile',
    views: {
      'menuContent': {
        templateUrl: 'templates/profile.html',
        controller: 'ProfileCtrl'
      }
    }
  })

  .state('app.signup', {
    url: '/signup',
    views: {
      'menuContent': {
        templateUrl: 'templates/signup.html',
        controller: 'UsersCtrl'
      },
      'fabContent': {
        template: ''
      }
    }
  })

  .state('app.ongList', {
    url: '/ongList',
    params: {
      obj: null
    },
    views: {
      'menuContent': {
        templateUrl: 'templates/ongList.html',
        controller: 'OngCtrl'
      },
      'fabContent': {
        template: ''
      }
    }
  })

  .state('app.sendQueue', {
    url: '/sendQueue',
    params: {
      entidade: null,
      idEntidade: null,
      total: null,
      logo: null,
      enviarListar: null,
      obj: null
    },
    views: {
      'menuContent': {
        templateUrl: 'templates/sendQueue.html',
        controller: 'SendQueueCtrl'
      },
      'fabContent': {
        template: ''
      }
    }
  });

  //Recupera os dados do localStorage. Esta em um service mas nao pode ser injetado aqui.
  /*userData = JSON.parse(window.localStorage.userData || '{}');
  var logado = userData.logado;

  console.log("Doa nota, app.js, primeira pagina. Logado: " + logado);

  if (logado != true) {
    $urlRouterProvider.otherwise('/app/login');
  } else {
    console.log('Tentou ir pro doa nota no otherwise');
    //$urlRouterProvider.otherwise('/app/doaNota');
  }*/
  // if none of the above states are matched, use this as the fallback

})

//Seta a url das apis DoaNota para ambiente de Debug e Producao
.constant('ApiEndpoint', {
  //url: 'http://192.168.0.115:8100/sistema'
  url: 'http://doanota.org/sistema/'
})
;
