/**
/**
 * Created by cristiano on 28/07/16.
 */
angular.module('DoaNotaCtrl', [])

.controller('DoaNotaCtrl', function($rootScope, $scope, $state, $ionicPlatform, $http, $stateParams, $timeout,
                                    ionicMaterialMotion, ionicMaterialInk, $ionicNavBarDelegate, $ionicSideMenuDelegate,
                                    $ionicPopup, $ionicLoading, UserService, $cordovaCamera, $cordovaFile, $cordovaBarcodeScanner,
                                    CameraService, CuponsService, $window, $log, $cordovaVibration, ConnectivityMonitor,
                                    $window, IntegrationData, $cordovaFileTransfer, $ionicHistory,
                                    CardsFactory, LocationService, $q) {

  $log.debug("controlador DoaNotactrl");

  $scope.controleDn = { };
  $scope.appName = IntegrationData.getAppName();

  //Armazena os parametros
  var varMultiplos = UserService.getUser().multiplos;
  var varCameraIniciar = UserService.getUser().cameraIniciar;
  var varEmail = UserService.getUser().email;
  var varEntidade  = IntegrationData.getEntity();
  var varLogo = IntegrationData.getLogo();
  var qtdCupons = 0;
  var globalCuponsCount = 0;
  var photoType = 1;
  var shareUrl;
  var origem = IntegrationData.getOrigin();

  var qtdCuponsTotal = 0;
  var varImageURI;
  var enviarListar   = 2;
  var entidade       = "";
  var logo           = "";
  var idEntidade     = IntegrationData.getEntityId();

  $rootScope.doRefresh = function () {
    $scope.doRefresh();
  }

  $scope.$on('$ionicView.beforeEnter', function(e) {
    //Atualiza a pagina a cada vez que entra
    //Funcao que evita que o evento seja disparado mais de uma vez.
    if(e.targetScope !== $scope) {
      return;
    }

    $log.debug("Evento BeforeEnter: chamada");
    if (ConnectivityMonitor.isOffline()) {

      var alertPopup = $ionicPopup.alert({
        title: "Você está sem Internet.",
        template: "Sem conexão, não é possível exibir a lista de cards. Reestabeleça a conexão para ver seu histórico.",
        cssClass: 'popupDoaNota-buttons'
      });

      var i = 0;

      $scope.cardList = [];

      $scope.cardList[i] = {
        index: i,
        data: null,
        entidade: "Sem conexão!",
        logo: "img/offlineCard.png",
        tempo: "Desconhecido"
      }

      $scope.$broadcast('scroll.refreshComplete');

    } else {
      $scope.doRefresh();
    }
  });

  setLayout();
  initializeCupomCount();
  getParameters();
  getLocation();

  var latitude;
  var longitude;

  function getLocation() {
    //Recupera a localidade
    LocationService.getLocation().then(

      function(res) {
        $rootScope.latitude  = res.latitude;
        $rootScope.longitude = res.longitude;
        $log.debug("Valores recuperados de Location " + $rootScope.latitude + " " + $rootScope.longitude);
      },
      function(err) {
        $log.error("Erro ao recuperar a localidade: " + err.message);
      }
    )
  };

  function initializeCupomCount() {
    //Apenas para testes de browser

    //Inicializa o objeto de controle e o campo de quantidade de cupons
    $scope.controleDn = {
      qtdCupons: getQtdCupons()
    };
  }

  function setLayout() {
    $timeout(function () {
      $scope.$parent.showHeader();

      //Ativa slide do menu
      $ionicSideMenuDelegate.canDragContent(true);
    }, 0);

    // Set Header
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.isExpanded = false;
    $scope.$parent.setExpanded(false);
    $scope.$parent.setHeaderFab(false);
    $scope.$parent.setHeaderFab('right');

    $scope.$root.showMenuIcon = true;
  }

  $scope.goRanking = function () {
    $state.go("app.ranking");
  }

  ////////////////////////////////////////
  // Methods de Compartilhamento
  ////////////////////////////////////////
  var shareOnSuccess = function(result) {
    $log.debug("Compartilhamento completado? " + result.completed); // On Android apps mostly return false even while it's true
    $log.debug("Compartilhado no App: " + result.app); // On Android result.app is currently empty. On iOS it's empty when sharing is cancelled (result.completed=false)
  }

  var shareOnError = function(msg) {
    $log.error("Compartilhamento falhou com a mensagem: " + msg);
  }

  $scope.cardShare = function(logo, entidade, quantidade, mensagem) {

    $cordovaVibration.vibrate(50);
    //Armazena a URL de Acordo com a plataforma
    if (ionic.Platform.isAndroid()) {
      shareUrl = IntegrationData.getAndroidShareUrl();
    } else {
      shareUrl = IntegrationData.getAppleShareUrl();
    }

    $log.debug("Logo do Share: " + logo);
    // this is the complete list of currently supported params you can pass to the plugin (all optional)
    var options = {
      //message: 'Fiz a doação de um cupom fiscal para a entidade com o Aplicativo ' + IntegrationData.getAppName() + '. Faça você também.', // not supported on some apps (Facebook, Instagram)
      message: mensagem,
      subject: mensagem,
      //Para Doar via Instagram, apenas com image.
      files: [logo], // an array of filenames either locally or remotely
      url: shareUrl,
      chooserTitle: 'Doação de Nota Fiscal'// Android only, you can override the default share sheet title
    }

    $log.debug("Click em cardShare");

    $window.plugins.socialsharing.shareWithOptions(options, shareOnSuccess, shareOnError);
    //$cordovaSocialSharing.share("Doe sua nota fiscal hoje com o Aplicativo DoaNota.", "img/DoaNotaLogin.jpg", "https://play.google.com/store/apps/details?id=org.apache.cordova.doanota");
  }

  $scope.doSharing = function () {
    $cordovaVibration.vibrate(50);

    //Armazena a URL de Acordo com a plataforma
    if (ionic.Platform.isAndroid()) {
      shareUrl = IntegrationData.getAndroidShareUrl();
      $log.debug("É Android, url de share é " + shareUrl);
    } else {
      shareUrl = IntegrationData.getAppleShareUrl();
      $log.debug("É IOS, url de share é " + shareUrl);
    }

    var options = {
      //Essa mensagem e importante apenas para email.
      message: 'Doe sua nota fiscal hoje com o Aplicativo ' + IntegrationData.getAppName() + '.', // not supported on some apps (Facebook, Instagram)
      subject: 'Doe sua nota fiscal hoje com o Aplicativo ' + IntegrationData.getAppName() + '.', // fi. for email

      //Para Doar via Instagram, apenas com image.
      //files: ["www/DoaNotaLogin.jpg"], // an array of filenames either locally or remotely
      url: shareUrl,
      chooserTitle: 'Ajude a ' + IntegrationData.getEntity() // Android only, you can override the default share sheet title
    }

    $window.plugins.socialsharing.shareWithOptions(options, shareOnSuccess, shareOnError);
    //$cordovaSocialSharing.share("Doe sua nota fiscal hoje com o Aplicativo DoaNota.", "img/DoaNotaLogin.jpg", "https://play.google.com/store/apps/details?id=org.apache.cordova.doanota");
  }

  function getParameters() {
    //Armazena os parametros
    varMultiplos = UserService.getUser().multiplos;
    varCameraIniciar = UserService.getUser().cameraIniciar;
    varEmail = UserService.getUser().email;
    qtdCupons = 0;
    photoType = 1;
  }

  $log.debug("Parametros no controlador doanota. Multiplos: " + varMultiplos + " camera: " + varCameraIniciar);

  //funcao para emitir alertas Ionic
  function alert(title, message) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: message,
      cssClass: 'popupDoaNota-buttons'
    });
  };

  $scope.$on('$ionicView.loaded', function(e) {
    $log.debug("IonicViewEnter, Camerainiciar= " + varCameraIniciar)

    if (varCameraIniciar == 1) {
      getParameters();
      initializeCupomCount();
      doarPhoto();
    };

  });

  //Timeout necessario para entrada rapida na tela, causando indisponibilidade antes da chamada do DeviceIsReady
  setTimeout(function() {
    if (varEmail != "") {
      if (ConnectivityMonitor.isOffline()) {

        //Nao carrega a conexao, carrega um card generico
        var i = 0;
        $scope.cardList = [];

        $scope.cardList[i] = {
          index: i,
          data: null,
          entidade: "Sem conexão!",
          logo: "img/offlineCard.png",
          tempo: "Desconhecido"
        }

      } else {
        //Carrega as ultimas doacoes
        try {
          carregaCardsDb();
        } catch (err) {
          //Erro ao carregar os cards termina a aplicação.
          $log.debug("Erro ao carregar os cards. Saindo da aplicação.")
          //ionic.Platform.exitApp()
        }
      }
    }
  }, 2000);

  //Rotina para carregar os cards.
  function carregaCardsDb(){
    $log.debug("Chamou a funcao para carregar os cards.");

    /*$ionicLoading.show({
     template: 'Carregando... ',
     duration: 10000
     });*/

    //Se estiver offline, nao chama a funcao
    if (ConnectivityMonitor.isOffline()) {

      var alertPopup = $ionicPopup.alert({
        title: "Você está sem Internet.",
        template: "Sem conexão, não é possível exibir a lista de cards. Reestabeleça a conexão para ver seu histórico.",
        cssClass: 'popupDoaNota-buttons'
      });

      var i = 0;

      $scope.cardList = [];

      $scope.cardList[i] = {
        index: i,
        data: null,
        entidade: "Sem conexão!",
        logo: "img/offlineCard.png",
        tempo: "Desconhecido"
      }

      $scope.isNotFirstCard = false;

      $scope.$broadcast('scroll.refreshComplete');

    } else {
      var db = $window.openDatabase("Database", "1.0", "DoaNotaDB", 200000);
      db.transaction(queryGetCardsDataDb, errorGetCardsDataDb);
    }
  }

  function queryGetCardsDataDb(tx) {
    //tx.executeSql("SELECT id, data, entidade, logo, tipo FROM ULTIMASDOACOES ORDER BY data DESC LIMIT 5", [], querySuccessLista, errorLista);
    tx.executeSql("SELECT data, entidade, quantidade, logo FROM ULTIMASDOACOES ORDER BY 1 DESC LIMIT 5", [], querySucessCardsList, errorCardsList);
  }

  function errorGetCardsDataDb(err) {
    alert("Erro SQL as ultimas 5 doacoes: " + err.message );
  };

  function errorCardsList(error) {
    alert("Erro SQL ao recuperar a lista das ultimas doacoes: " + error.message );
  }

  function calcCardTime(databaseDate) {
    var tempo = "";

    var date2 = Date.now();
    var date1 = new Date(Date.parse(databaseDate.replace(" ", "T") + "-02:00"));
    $log.debug("date1(banco) é : " + date1 + ", data now é: " + date2);

    var diff = date2.valueOf() - date1.valueOf();
    var diffInHours = diff/1000/60/60; // Convert milliseconds to hours
    var diffInHours = diff/1000/60/60; // Convert milliseconds to hours

    if (diffInHours > 720) {
      $log.debug("há " + Math.ceil(diffInHours / 720) + " meses");
      tempo = "há " + Math.ceil(diffInHours / 720) + " meses";

    } else if (diffInHours > 168) {
      $log.debug("há " + Math.ceil(diffInHours / 168) + " semanas");
      tempo = "há " + Math.ceil(diffInHours / 168) + " semanas";
    } else if (diffInHours > 24) {
      $log.debug("há " + Math.ceil(diffInHours /24) + " dias");
      tempo = "há " + Math.ceil(diffInHours /24) + " dias";
    } else {
      if (Math.ceil(diffInHours) > 1 ) {
        $log.debug("há " + Math.ceil(diffInHours) + " horas");
        tempo = "há " + Math.ceil(diffInHours) + " horas";
      } else {
        if (diffInHours < '0.50') {
          $log.debug("agora");
          tempo = "agora";
        } else {

          $log.debug("há " + Math.ceil(diffInHours) + " hora");
          tempo = "há " + Math.ceil(diffInHours) + " hora";
        }
      }
    }

    return tempo;

  }

  function querySucessCardsList(tx, results) {

    var total = results.rows.length;
    //var menorId;
    $log.debug("query doacoes cards com sucesso, quantidade: " + total);

    /*$ionicLoading.show({
      template: '<ion-spinner></ion-spinner> <span>  Carregando...  </span>',
      duration: 10000
    });*/

    if(total >= 1) {

      $scope.cardList = [];

      for (var i = 0; i < total; i++) {
        //Efetua a chamada para recuperar o dados do Card


        var promise = CardsFactory.getCardData(results.rows.item(i).quantidade, results.rows.item(i).data, i);

        //Funcao que aguarda a promessa. Somente aqui as variaves tem valor, fora se tornam assincronas
        promise.then(function(result) {

          i = i-1;

          databaseDate = result.data;//results.rows.item(i).data;
          var tempo = calcCardTime(databaseDate);

          var cardLogo = "";
          var cardMessage = "";
          var cardLink = "";

          var jsonResponse = $scope.$eval(result.response.data);//angular.fromJson(jsonData[1]);
          $log.debug("Resposta para o CardsFactory: " + JSON.stringify(jsonResponse));

          //Armazena os pontos de resposta
          cardLogo = jsonResponse.imagem;
          cardMessage = jsonResponse.mensagem;
          cardLink = jsonResponse.link;

          $scope.cardList[i] = {
            index: result.index,
            //data: Date.parse(results.rows.item(i).data),
            data: new Date(Date.parse(databaseDate.replace(" ", "T") + "-02:00")),
            entidade: results.rows.item(i).entidade,
            logo: cardLogo,
            tipo: results.rows.item(i).tipo,
            quantidade: result.quantidade,//results.rows.item(i).quantidade,
            tempo: tempo,
            mensagem: cardMessage
          }
        });
      }////for (var i = 0; i < total; i++) {


      $scope.isNotFirstCard = true;

      //$log.debug("Menor id " + menorId);
      $scope.$broadcast('scroll.refreshComplete');
      //$ionicLoading.hide();

    } else {
      //$ionicLoading.hide();
      //Mostra o card default caso nao exista.
      var i = 0;

      $scope.cardList = [];

      $scope.cardList[i] = {
        index: i,
        data: null,
        entidade: "Bem vindo ao " + $scope.appName + "!",
        logo: "img/firstCard.png",
        tempo: "Agora"
      }

      $scope.isNotFirstCard = false;
      $scope.$broadcast('scroll.refreshComplete');
    }
  }

  /**********************************
   ****Inicio  Rotinas da Camera ****
   **********************************/
  $scope.pictureUrl = 'http://placehold.it/300x300';

  $scope.takePicture = function() {

    $log.debug("Entrou para TakePicture, tirar foto.");
    //Recupera os parametros antes de tirar foto
    getParameters();
    doarPhoto();
  };

  $scope.getPicture = function (options) {
    var options = {
      quality : 50,
      targetWidth: 300,
      targetHeight: 300,
      sourceType: 0
    };

    $cordovaCamera.getPicture(options).then(function(imageData) {
      $log.debug('Entrou em get picture.');
      $scope.pictureUrl = imageData;
    }, function(err) {
      $log.error(err);
    });

  };

  function doarPhoto() {
    $log.debug("Funcao doarPhoto");
    var options = {
      destinationType: Camera.DestinationType.FILE_URI,
      encodingType: Camera.EncodingType.JPEG,
      quality: 75,
      //targetWidth: 450,
      //targetHeight: 450,
      sourceType: 1
    }

    $cordovaCamera.getPicture(options)
      .then(function(data) {

        $log.debug('Camera data: ' + angular.toJson(data));

        imageURI = data;
        $log.debug(imageURI);

        var sourceDirectory = imageURI.substring(0, imageURI.lastIndexOf('/') + 1);
        var sourceFileName = imageURI.substring(imageURI.lastIndexOf('/') + 1, imageURI.length);


         //Create a new name for the photo
        var d = new Date(),
            n = d.getTime(),
            newFileName = n + ".jpg";

        //Caso seja ios, copia o arquivo para o local persiste
        if (ionic.Platform.isIOS()) {

          $cordovaFile.copyFile(sourceDirectory, sourceFileName, cordova.file.dataDirectory, newFileName).then(

            function(success) {
              //$scope.fileName = cordova.file.dataDirectory + sourceFileName;
              gotFileEntryDataDir(cordova.file.dataDirectory + newFileName);
            },
            function(msg) {
              $log.error("Erro copiando o arquivo de imagem para diretorio persistente ios: " + msg.error);
            }
          );

        } else {

          //Resolve/Recupera o endereco do file storage local
          $window.resolveLocalFileSystemURL(imageURI, gotFileEntry, fail);

        }

      }, function(err) {

        //Camera fechada, chama o processo de listar as Entidades
        CuponsService.getQtdCupons().then(

          function(result) {
            if (result > 0 ) {
              $log.debug("Quantidade de Cupons: " + result);
              showEntidades();
            }

          }
        )

      });
    //onSuccessGrava(data), onFailPhoto(err));
  };

  /********************************
   **** Fim Rotinas da Camera  ****
   ********************************/

  /**********************************
   **** Inicio Rotinas de QR Code ***
   **********************************/
  $scope.takeQRCode = function () {

    //Recupera os parametros antes de tirar foto
    getParameters();
    doarPhotoQr();

  };
  /***********************************
   **** Fim das Rotinas de QR Code ***
   ***********************************/
  function doarPhotoQr() {

    var options = {
      showTorchButton : true, // iOS and Android
      prompt : "Posicione o QRCode da Nota Fiscal ao centro.", // Android
      resultDisplayDuration: 500, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
      formats : "QR_CODE" // default: all but PDF_417 and RSS_EXPANDED
    }

    //Funcao que valida se o QRcode esta em formato correto
    function validateQRCode(text) {
      var re = /([^\d]|^)\d{44}([^\d]|$)/;
      return re.test(text);
    };

    $cordovaBarcodeScanner
      .scan(options)
      .then(function(imageData) {
        $log.debug("QRCode escanneado com sucesso: " + imageData.text);

        if (imageData.text != "") {
          if (validateQRCode(imageData.text)) {
            gravaBanco('', imageData.text);
          } else {

            var invalidQrCode = $ionicPopup.confirm({
              title: "QRCode Inválido",
              template: "O QRCode lido não tem 44 posições numéricas. Escaneie novamente um QRCode válido.",
              cssClass: 'popupDoaNota-buttons',
              cancelText: 'Cancelar',
              okText: 'Continuar'
            });

            invalidQrCode.then(function(res) {
              if(res) {
                $log.debug('Confirmado continuar leitura.');
                doarPhotoQr();
              } else {
                $log.debug('Nao confirmado, clicado em cancelar.');

                CuponsService.getQtdCupons().then(
                  function(result) {
                    if (result > 0) {
                      showEntidades();
                    }
                  }
                );
              }
            });
          };


        } else {

          CuponsService.getQtdCupons().then(
            function(result) {
              if (result > 0) {
                showEntidades();
              }
            }
          );

        }

      }, function(error) {
        $log.error("Erro escaneando QRCode: " + error);
      });
  };

  function gotFileEntryDataDir(fileEntry) {

    $log.debug("Entrou em gotFileEntryDataDir: " + fileEntry);

    //Chama a funcao para gravar os dados no banco
    gravaBanco(fileEntry,'');

  }
  function gotFileEntry(fileEntry) {

    $log.debug("Entrou em getFileEntry: " + fileEntry.nativeURL);

    //Chama a funcao para gravar os dados no banco
    gravaBanco(fileEntry.nativeURL,'');
  }

  function fail(msg){
    alert('Erro ao gravar/recuperar Imagem, resolvendo URI local:' + msg);
  }

  function onSuccessGrava(data) {
    $log.debug("Imagem URI recuperada: " + angular.toJson(data));

  };

  function onFailPhoto(err) {
    $log.debug("Erro no processo de tirar foto: " + err);
  };

  function gravaBanco(imageUri, QrCode) {

    var db = $window.openDatabase("Database", "1.0", "DoaNotaDB", 200000);
    db.transaction(function(tx) {

      //CuponsService.setQtdCupons(CuponsService.getQtdCupons() + 1);
      tx.executeSql('INSERT INTO ENVIANOTA (data, foto, status, qrcode ) VALUES ' +
                    '(datetime("now","localtime"),?,?,?)',[imageUri,"0",QrCode],multishowEntidades,errorHandlerImage);

      $log.debug("grava banco, varMultiplos: " + varMultiplos);

        if(varMultiplos == 1){
          //PhotoType = 1 indica foto de camera, e nao da galeria (futuro)
          if(	photoType ==1 ){
            if(QrCode==""){
              doarPhoto();
            } else {
              doarPhotoQr();
            }
          } else {
            doarphotofile();
          }

        } else {

          qtdCupons = qtdCupons + 1;

          if(QrCode==""){
            //a funcao show entidades ja direciona o cupom diretamente, sem chamar a lista
            //showEntidades('');
          } else {
            //showEntidades('qrcode');
          }
        }
    }, erroRemove //Error Function
    )
  };

  function multishowEntidades(){

    globalCuponsCount = globalCuponsCount + 1;
    $log.debug("CONTADOR Global: " + globalCuponsCount);

    if(varMultiplos !=1 ){
      showEntidades('multishow');
    };
  }

  function erroRemove(err){
    $log.error("Erro: Remove: " + err )
  }

  $scope.showEntidadesTeste = function() {
    //qtdCupons = 1;
    showEntidades('');
  }

  function showEntidades(fromwhere) {

    $log.debug("Entrou na listagem de entidades. ShowEntidades");

    var qtdCupons;

    //Recupera a quantidade de cupons do servico
    CuponsService.getQtdCupons().then(function (result) {
      qtdCupons = result;

      if (qtdCupons > 0) {
        $log.debug("if mostrando que a quantidade de cupons é maior que zero.");

        //Vai para a pagina de entidades
        //$scope.doRefresh();
        initializeCupomCount();

        if (ConnectivityMonitor.isOffline()) {

          var alertPopup = $ionicPopup.alert({
            title: "Você está sem Internet.",
            template: "Sem conexão, não é possível enviar os cupons à " + IntegrationData.getEntity() + ". Não se preocupe: " +
            "Suas notas ficarão armazenadas e você pode enviar quando voltar a ficar on-line."
            ,
            cssClass: 'popupDoaNota-buttons'
          });

        } else {

          sendControl();
        }
      } else {

        //Terminando vai para a home
        $state.go('app.doaNota');
      }
    })
  };

  function sendControl() {
    entidade = varEntidade;
    logo     = varLogo;

    idEntidade = IntegrationData.getEntityId();
    var qtdCupons = 0;
    //Chama o Service que recupera a quantidade do armazenamento local.

    CuponsService.getQtdCupons().then(

      function(result) {
        qtdCupons =  result;

        var confirmDonation = $ionicPopup.confirm({
          title: "Envio de Cupons",
          template: 'Confirma doação de ' + qtdCupons  + ' Cupom(s)',
          cssClass: 'popupDoaNota-buttons',
          cancelText: 'Listar',
          okText: 'Enviar'
        });

        confirmDonation.then(function(res) {
          if(res) {
            $log.debug('Confirmado envio de cupons.');
            onConfirmEntidade();
          } else {
            $log.debug('Nao confirmado, clicado em listar.');
            onListarCupons(entidade);
          }
        });

      });
  };//function marcaEntidade(varEntidade){

  function errorHandlerQtd(transaction, error) {
    alert('Erro', 'Erro ao consultar os cupons: ' + error.message + ' code: ' + error.code);
  }


  function errorHandlerImage(transaction, error) {
    $log.error(">>>"+ error.message + ' code: ' + error.code);
    alert('Error: ' + error.message + ' code: ' + error.code);

  };

  function querySuccessListaImagens(tx, results) {

    var total = results.rows.length;
    if(total>=1){


      var html = '<div data-role="collapsible" data-inset="false">';

      html += '<h2> Total de ' + total + ' Fotos</h2>';

      for (var i=0;i<total;i++) {

        html += '<hr>Data:' + results.rows.item(i).data + '<br>' + results.rows.item(i).foto ;
      }

    } else {

      $scope.controleDn.qtdCupons = 3;

    };
  };

  function getQtdCupons() {

    var q = $q.defer();

    var qtd = 0;
    //Chama o Service que recupera a quantidade do armazenamento local.
    CuponsService.getQtdCupons().then(

      function(result) {
        $log.debug("recebido retorno, quantidade: " + result);
        qtd = result;

        //Armazena o contador global para controle de envio
        globalCuponsCount = qtd;

        showHideQtdCupom(qtd);
        q.resolve(qtd);

        return q.promise;
      }

    );

  }

  function showHideQtdCupom(varQtd){

    if(varQtd==0 || varQtd==null){
      $scope.controleDn.qtdCupons = 0;

      $log.debug("Valor no Escopo quando zero: " + $scope.controleDn.qtdCupons);
    } else {
      $scope.controleDn.qtdCupons = varQtd;

      $log.debug("Valor no Escopo doaNota: " + $scope.controleDn.qtdCupons);

    }
  }

  $rootScope.onConfirmEntidade = function() {

    $log.debug("Root onConfirmEntidade");
    onConfirmEntidade();
  }


  function onConfirmEntidade() {
    enviarListar = 1;
    $log.debug("Envio de Cupons, chama enviarCuponsUpload(). O id da entidade é " + idEntidade);
    enviarCuponsUpload(1);
  }

  function onListarCupons() {

    //alert("Listar, Implementar aqui a funcao de listar.");
    enviarListar = 2;
    enviarCuponsUpload();

  }

  function enviarCuponsUpload(){
    var db = $window.openDatabase("Database", "1.0", "DoaNotaDB", 200000);
    db.transaction(queryDB, errorDB);
  }

  function queryDB(tx) {
    qtdCuponsTotal = getQtdCupons();
    tx.executeSql("SELECT strftime('%d-%m-%Y %H:%M:%S',data) as data,foto,qrcode FROM ENVIANOTA ", [], querySuccessLista, errorLista);
  }

  function errorDB(err) {
    alert("Erro SQL ao recuperar as notas: " + err.message );
    return;
  };

  function querySuccessLista(tx, results) {

    $log.debug("Query com sucesso, depois do Select notas, quantidade: " + results.rows.length);

    var total = results.rows.length;

    $log.debug("Tamanho do Banco: " + total);

    if(total >= 1){

      $scope.queue = [];

      for(var i=0;i<total;i++) {

        varimageURI = results.rows.item(i).foto;

        var status;
        if (enviarListar == 1) {
          status = "Enviando";
        } else {
          status = "Na Fila";
        }

        $scope.queue[i] = {
          index: i,
          foto: results.rows.item(i).foto,
          data: results.rows.item(i).data,
          qrcode: results.rows.item(i).qrcode,
          status: status
        }
      }

      if (enviarListar == 2) {
        $log.debug("Listar entidades.");
        $state.go('app.sendQueue',  {entidade: entidade, idEntidade: idEntidade, total: total, logo: logo, enviarListar: enviarListar, obj: $scope.queue});
      }

      if(enviarListar==1){
        for(var i=0;i<total;i++) {
          $log.debug("Enviando, chamado tela da Fila");
          $state.go('app.sendQueue',  {entidade: entidade, idEntidade: idEntidade, total: total, logo: logo, enviarListar: enviarListar, obj: $scope.queue});
          //uploadPhoto(results.rows.item(i).foto,results.rows.item(i).qrcode,i,total,entidade,idEntidade);
        }

      }
    } else {
      alert("Sem Cupons", "Não há cupons à serem exibidos.");
    }
  };

  function errorLista(err) {
    alert("Erro", "Erro ao recuperar a lista de cupons: " + err.message)
  };

  function removeSqlNOk(error){
    $log.debug("Erro executando delete de cupons " + error.message);
  }

  //Funcao para carregar a pagina de cards
  $scope.doRefresh = function () {
    setLayout();
    initializeCupomCount();
    getParameters();
    carregaCardsDb();
  }

})

.controller('SendQueueCtrl', function($rootScope, $scope, $state, $http, $window, $ionicModal, CuponsService,
                                      $ionicPopup, $ionicLoading, $ionicHistory, $log, UserService,
                                      IntegrationData, $cordovaFileTransfer, $cordovaGeolocation,
                                      CuponsHistoryService, $q, LocationService, $timeout, ApiEndpoint, jQueryLikeSerializeFixed) {

  $log.debug("Entrou no controlador SendQueueCtrl");

  //Variaveis globais
  var getErros = 0;
  var entidade = IntegrationData.getEntity();

  $scope.$on('$ionicView.beforeEnter', function(e) {
    //Atualiza a pagina a cada vez que entra
    updateQueueData();
  });

  updateQueueData();

  function getQtdCupons() {

    var q = $q.defer();

    var qtd = 0;
    //Chama o Service que recupera a quantidade do armazenamento local.
    CuponsService.getQtdCupons().then(

      function(result) {
        $log.debug("recebido retorno, quantidade: " + result);
        qtd = result;

        q.resolve(qtd);

        return q.promise;
      }
    );
  }

  function updateQueueData() {

    $scope.queue = [];

    angular.copy($state.params.obj, $scope.queue);

    $scope.header = {};
    $scope.header = {
      ongName: $state.params.entidade,
      ongId: $state.params.idEntidade,
      total: $state.params.total,
      logo: $state.params.logo,
      qtdCupons: $state.params.total
    }

  }

  //funcao para emitir alertas Ionic
  function alert(title, message) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: message,
      cssClass: 'popupDoaNota-buttons'
    });
  };

  $ionicModal.fromTemplateUrl('image-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $log.debug("instanciou o modal");
    $scope.modal = modal;
  }, function(err) {
    $log.error("Erro ao instanciar o modal. Não instanciou o modal: ");
  });

  $scope.openModal = function() {
    $scope.modal.show();
  };

  $scope.closeModal = function() {
    $scope.modal.hide();
  };

  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    //$scope.modal.remove();
    $log.debug("Ocorreu o destroy");
  });
  // Execute action on hide modal
  $scope.$on('modal.hide', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });
  $scope.$on('modal.shown', function() {
    $log.debug('Modal is shown!');
  });

  $scope.showImage = function (imageUri, tipo) {

    if (imageUri=="") {
      $scope.imageSrc = "img/qrCodeDoaNota.jpg"
    } else {
      $scope.imageSrc = imageUri;
    }

    //break;
    $log.debug("Clicou na imagem, ImageURI é " + $scope.imageSrc);

    $scope.openModal();
  }

  //var len = queue.length;
  var len = $scope.queue.length;
  $log.debug('Tamanho do array de Lista de cupons ' + len);

  //Se for operacao de envio ja inicia o envio
  if ($state.params.enviarListar == 1) {
    sendCupons($state.params.entidade, $state.params.idEntidade, $state.params.logo);
  }

  $scope.sendCupomList = function(entidade, idEntidade, logo) {
    sendCupons(entidade, idEntidade, logo);
  }

  function sendCupons(entidade, idEntidade, logo) {
    //var len = $scope.queue.length;
    var total = $state.params.total;

    // o valor de total esta fora do array
    // os demais estao no array.. veja o objeto na chamada

    //Armazena o total recebido por parametro do acumulador doanota.
    totalCupons = total;

    //Varre a lista de cupons baseado no parametro de total
    for(var i=0;i<total;i++) {

      $scope.queue[i].status = "Enviando";
      uploadPhoto($scope.queue[i].foto,$scope.queue[i].qrcode,i,total,entidade,idEntidade, logo);

    }
    //$log.debug("Enviando cupons SendQueue");
  }

  $scope.removeCupons = function () {

    var confirmRemove = $ionicPopup.confirm({
      title: "Excluir Cupons",
      template: 'Deseja remover TODOS os cupons?',
      cssClass: 'popupDoaNota-buttons',
      cancelText: 'Cancelar',
      okText: 'Excluir'
    });

    confirmRemove.then(
      function(res) {
        if (res) {
          $log.debug("Apagar todos cupons, clicado em ok, vai chamar o banco.");

          $ionicLoading.show({template: "Excluindo cupons..."});

          var db = window.openDatabase("Database", "1.0", "DoaNotaDB", 200000);
          db.transaction(removeDB, errorDB, goToHome);
        } else {
          $log.debug("Apagar todos cupons, clicado em Cancelar.");
        }
      });
  }//var confirmRemove = $ionicPopup.confirm({

  function removeDB(tx) {
    tx.executeSql('delete from ENVIANOTA');
    $ionicLoading.hide();

    alert("Cupons Excluídos", "Cupons excluídos com sucesso");
    $rootScope.doRefresh();

  };

  function errorDB(err) {
    alert("Erro SQL ao recuperar as notas: " + err.message );
    return;
  };

  function goToHome() {

    $ionicHistory.nextViewOptions({
      disableBack: true
    });
    $state.go('app.doaNota');
  }

  function uploadPhoto(imageURI,qrcode,i,total,entidade,idEntidade,logo) {

    var params = {};

    //Parametros do dispositivo
    var deviceInformation = ionic.Platform.device();
    params.uuid           = deviceInformation.uuid;
    params.name           = deviceInformation.model;
    params.version        = deviceInformation.version;

    //Dados da plataforma
    params.platform        = ionic.Platform.platform();

    //Dados da Tela
    params.screenwidth     = $window.innerWidth;
    params.screenheight    = $window.innerHeight;

    //Usuario
    params.email           = UserService.getUser().email;

    params.lat             = $rootScope.latitude;
    params.long            = $rootScope.longitude;

    //Parametros
    params.caminho         = imageURI;
    params.id_entidade     = idEntidade;
    params.qrcode          = qrcode;
    params.origem          = IntegrationData.getOrigin() + '-' + IntegrationData.getAppVersion();


    $log.debug("Entrou no service para enviar dados " + idEntidade);
    $log.debug("Preparando envio, id da entidade é: " + idEntidade);

    if (imageURI == "") {
      $log.debug("Image nao encontrada. Rotina de envio QRCode.");

      //Carrega a variavel de configuracao
      var config = {
        parametros: params
      };

      //Carrega a url e concatena com a lista de parametros preparadas como jquery.
      var url = ApiEndpoint.url + "/receive_qrcode.php?JSON_CALLBACK=?" + jQueryLikeSerializeFixed(config) ;


      $http.get(url).then(
        function (response) {

          $scope.queue[i].status = "Sucesso!";

          $log.debug("Resposta com sucesso: " + response.data);
          var jsonResponse = $scope.$eval(response.data);//angular.fromJson(jsonData[1]);

          //Valida a resposta do servidor
          if (jsonResponse.validacion == "") {

            //Exibe a mensagem de erro.
            alert("Erro no Envio", "Erro ao enviar os dados do QRCode, Cupom " + i + ", à " + IntegrationData.getEntity() + ". Certifique-se que os cupons lidos são QRCodes válidos." +
              " Exclua os cupons inválidos e tente novamente. ");

            $scope.queue[i].status = "Erro";

            //Prepara o envio do QRCode Puro
            var datetime = new Date().toLocaleString();

            //Carrega os parametros
            var config = {
              params: {
                hash: "doanota",
                tipo: "entidade",
                id_entidade: idEntidade,
                email: UserService.getUser().email,
                data: datetime,
                qrcode: qrcode
              }
            };

            //Chama API Para logar o QRCode puro e acompanhamento
            $log.debug(config);
            //Armazena a url e config para cards
            var url = ApiEndpoint.url + "/jsonlog.php?"

            $http.get(url, config).then(
              function(response) {
                $log.debug("Enviado o log de QRCode com sucesso.");
              },

              function(response) {
                $log.error("Erro chamado a URL para envio de Log de QRCode")
              }
            );

          } else {

            $log.debug("Envio dos dados de QRCode com sucesso. Resposta é: " + response.data);

            removeFile(qrcode,'qrcode',i,total);
          }

        },//Success Callback

        function (response) {

          $log.error("Erro ao enviar os dados de QRCode ao DoaNota(GET). Status: " + response.status + ", response: " + response.data);

          $scope.queue[i].status = "Erro";

          //Acumula a Variável de erros
          getErros = getErros + 1;

        }//function (error) {
      )


    } else {
      $log.debug("ImageURI encontrada. Rotina de envio Imagem.");

      varImageURI = imageURI;

      var options = new FileUploadOptions();

      options.eKey = "file";
      options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
      options.mimeType = "image/jpeg";
      options.params = params;
      options.chunkedMode = false;

      $cordovaFileTransfer.upload("http://doanota.org/img/upload.php", imageURI, options)
        .then(
          function (result) {
            $log.debug("Sucesso ao enviar a imagem, i=" + i + " total=" + total);

            if (i == total - 1) {
              $scope.loading = false;
            }

            $scope.queue[i].status = "Sucesso!";

            removeFile(params.caminho,'foto',i,total);

          },
          function (err) {
            $log.error("Erro enviando a imagem." + err.message);

            $scope.queue[i].status = "Erro";
            //Acumula a variável de erros
            getErros = getErros + 1;
          },
          function (progress) {
            $log.debug("Enviando a imagem.");

            //Implementar GIF?
            $log.debug("Chamou Enviando, indice " + i + " total " + total);

            $scope.loading = true;
          }
        );

      return true;
    };
  }

  $scope.showPaperIcon = function(status) {
    if (status == "Na Fila") {
      return true
    } else {
      return false
    }
  }

  $scope.showCompletionIcon = function(status) {
    if (status == "Sucesso!") {
      return true
    } else {
      return false
    }
  }

  $scope.showLoadingIcon = function(status) {
    if (status != "Sucesso!" && status != "Na Fila" && status != "Erro") {
      return true
    } else {
      return false
    }
  }

  $scope.showErrorIcon = function(status) {
    if (status == "Erro") {
      return true
    } else {
      return false
    }
  }

  function removeFile(caminho,tipo,i,total) {

    var db = $window.openDatabase("Database", "1.0", "DoaNotaDB", 1024 * 1024);

    db.transaction(function (tx) {

      if(tipo=='qrcode'){
        var varQuery = 'delete from ENVIANOTA WHERE qrcode = "' + caminho + '"';
      } else {
        var varQuery = 'delete from ENVIANOTA WHERE foto = "' + caminho + '"';
      }

      /*var qtd = CuponsService.get;
      CuponsService.setQtdCupons(parseInt(qtd)-1);*/

      tx.executeSql(varQuery,[],function (r){
        $log.debug("Remocao dos cupons com sucesso. VALOR do total de CUPONS: " + i + " total geral: " + totalCupons);

        totalCupons = totalCupons -1;

        }
        ,removeSqlNOk);

      if (totalCupons == 1) {
        validaFimEnvio(totalCupons);
      }

    }, errorCallback, removeOk());
  //	db.transaction(removeImageQuery, erroremove, removeOk);

  };

  function validaFimEnvio(qtd){

    //Chama o Service que recupera a quantidade do armazenamento local.
    $log.debug("SendQueue Quantidade de Cupons Final recebida é " + qtd);

    if(qtd==1){

      qtdCuponsTotal=0;

      if( getErros == 0 ){

        //Desativa a navegacao da proxima tela.
        $ionicHistory.nextViewOptions({
          disableBack: true
        });


        //Insere o historico em caso de sucesso
        CuponsHistoryService.insertHistory($state.params.total, $state.params.idEntidade, $state.params.logo, "cupom");

        $state.go("app.doaNota");

        alert("Obrigado", "A " + entidade + " agradece a sua ajuda!");
        qtdCuponsTotal=0;



      } else {

        alert("Erros", "Faltaram " + getErros + " cupons a serem enviados.");

      }
    }
  };


  function errorCallback(error){
    $log.error("Erro preparando statement delete de cupons " + error.message);
  }

  function removeSqlNOk(error){
    $log.debug("Erro executando delete de cupons " + error.message);
  }

  function removeOk(){

  }

});

