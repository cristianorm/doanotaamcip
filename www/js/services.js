angular.module('doaNota.services', [])

.service('UserService', function($log) {
    // For the purpose of this example I will store user data on ionic local storage but you should save it on a database
    var setUser = function(user_data) {
      window.localStorage.userData = JSON.stringify(user_data);
    };

    var getUser = function(){
      return JSON.parse(window.localStorage.userData || '{}');
    };

    var delUser = function(userID) {
      window.localStorage.clear();
      $log.debug("Removido o usuario " + userID );
    }

    return {
      getUser: getUser,
      setUser: setUser,
      delUser: delUser
    };
  }
)//.service('UserService', function() {

.factory('CameraService', function ($q) {

  return {
    getPicture: function (options) {
      var q = $q.defer();

      navigator.camera.getPicture(function (result) {
          q.resolve(result);
        }, function (err) {

          q.reject(err);
        }, options
      ); //navigatorGetPicture

      return q.promise;

    }//getPicture
  }//return
})

.service('LocationService', function($cordovaGeolocation, $log, $q, $timeout) {

   var getLocation = function getLocation() {

      var defer = $q.defer();

      var location = {};

      function successLocation(p) {

        location = {
          latitude: p.coords.latitude,
          longitude: p.coords.longitude
        }

        defer.resolve(location);

      };

      function locationFail(error){
        $log.error("Falha ao recuperar a localizacao: " + error.message);

        defer.reject("Falha ao recuperar a localizacao: " + error.message);
      };

      $cordovaGeolocation.getCurrentPosition().then(
        successLocation,
        locationFail
      );

     return defer.promise;
    };

    return {
      getLocation : getLocation
    }
})

.service('CuponsHistoryService', function ($window, $log, $ionicPopup) {

  //funcao para emitir alertas Ionic
  function alert(title, message) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: message,
      cssClass: 'popupDoaNota-buttons'
    });
  };

  function historySuccessDB() {
    $log.debug("Inseriu o historico na tabela ULTIMASDOACOES.");
  }

  function errorHistoryDB(err) {
    $log.error("Não inseriu o historico na tabela Ultimas Doaçoes. Verifique " + err.message)
    alert("Erro Historico", "Erro ao inserir o histórico de Ultimas Doacoes: " + err.message);
  };

  function errorCallbackHistory(error){
    $log.error("Erro preparando statement de Inserir Historico de cupons " + error.message);
  }

  function removeOk(){

  }

  var insertHistory = function insertHistoryDb(total, entidade, logo, tipo) {

    var db = $window.openDatabase("Database", "1.0", "DoaNotaDB", 1024 * 1024);

    $log.debug("variaveis: entidade" + entidade + ", logo " + logo + ", tipo " + tipo);

    db.transaction(function (tx) {
      $log.debug("Entrou para registrar o historico. Total de Cupons " + total);
      //Criar os registros para suportar os cards
      tx.executeSql('INSERT INTO ULTIMASDOACOES (data, entidade, quantidade, logo, tipo ) VALUES ' +
        '(datetime("now","localtime"),?,?,?,?)', [entidade, total, logo, tipo], historySuccessDB, errorHistoryDB);
    }, errorCallbackHistory, removeOk());
  }

  return {
    insertHistory : insertHistory
  }


})

.service('CuponsSendService', function($http, $window, $cordovaFileTransfer, $ionicLoading, $log, $ionicPopup, jQueryLikeSerializeFixed, UserService, CuponsService, GetLocationService, IntegrationData, ApiEndpoint) {

  //funcao para emitir alertas Ionic
  function alert(title, message) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: message,
      cssClass: 'popupDoaNota-buttons'
    });
e
    alertPopup.then(function (res) {
      $log.debug('Click alert, titulo: ' + title + ', Mensagem: ' + message);
    });
  };

  var uploadPhoto = function uploadPhoto(imageURI,qrcode,i,total,entidade,idEntidade) {

    var params = {};

    //Parametros do dispositivo
    var deviceInformation = ionic.Platform.device();
    params.uuid           = deviceInformation.uuid;
    params.name           = deviceInformation.model;
    params.version        = deviceInformation.version;

    //Dados da plataforma
    params.platform        = ionic.Platform.platform();

    //Dados da Tela
    params.screenwidth     = $window.innerWidth;
    params.screenheight    = $window.innerHeight;

    //Usuario
    params.email           = UserService.getUser().email;

    //Localidade
    params.lat             = GetLocationService.getLocation().latitude;
    params.long            = GetLocationService.getLocation().longitude;

    //Parametros
    params.caminho         = imageURI;
    params.id_entidade     = idEntidade;
    params.qrcode          = qrcode;
    params.origem          = IntegrationData.getOrigin();;

    var totalCupons = CuponsService.getQtdCupons();

    $log.debug("Entrou no service para enviar dados " + idEntidade);
    $log.debug("Preparando envio, id da entidade é: " + idEntidade);

    if (imageURI == "") {
      $log.debug("Image nao encontrada. Rotina de envio QRCode.");

      //Implementar GIF ?
      $log.debug("Chamou Enviando QR, indice " + i + " total " + totalCupons);

      $ionicLoading.show({
          template: '<ion-spinner></ion-spinner> <span>  Enviando ' + totalCupons + ' cupom(s)...  </span>',
          duration: 30000
        }

      );

      //Carrega a variavel de configuracao
      var config = {
        parametros: params
      };

      //$log.debug("Variável config: " + JSON.stringify(config));

      //Carrega a url e concatena com a lista de parametros preparadas como jquery.
      var url = ApiEndpoint.url + "/receive_qrcode.php?JSON_CALLBACK=?" + jQueryLikeSerializeFixed(config) ;

      $http.get(url).then(
        function (response) {

          $log.debug("Resposta com sucesso: " + response.data);

          //var jsonResponse = $scope.$eval(response.data);//angular.fromJson(jsonData[1]);
          var jsonResponse;

          if (i == total -1 ) {
            $ionicLoading.hide();
          };

          //Valida a resposta do servidor
          if (jsonResponse.validacion == "") {

            alert("Erro no Envio", "Erro ao enviar os dados QRCode ao DoaNota. Tente novamente.");
            //$state.go("app.doaNota");

          } else {

            $log.debug("Envio dos dados de QRCode com sucesso. Resposta é: " + response.data);

            removeFile(qrcode,'qrcode',i,total);

          }

        },//Success Callback

        function (response) {

          $log.error("Erro ao enviar os dados de QRCode ao DoaNota(GET). Status: " + response.status + ", response: " + response.data);

          if (i == total -1 ) {
            $ionicLoading.hide();
          };
          //Acumula a Variável de erros
          getErros = getErros + 1;

        }//function (error) {
      )


    } else {
      $log.debug("ImageURI encontrada. Rotina de envio Imagem.");

      varImageURI = imageURI;

      var options = new FileUploadOptions();

      options.eKey = "file";
      options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
      options.mimeType = "image/jpeg";
      options.params = params;
      options.chunkedMode = false;

      $cordovaFileTransfer.upload("http://doanota.org/img/upload.php", imageURI, options)
        .then(
          function (result) {
            $log.debug("Sucesso ao enviar a imagem, i=" + i + " total=" + total);

            if (i == total - 1) {
              $scope.loading = false;
            }

            removeFile(params.caminho,'foto',i,total);

          },
          function (err) {
            $log.error("Erro enviando a imagem." + err.message);

            //Acumula a variável de erros
            getErros = getErros + 1;
          },
          function (progress) {
            $log.debug("Enviando a imagem.");

            //Implementar GIF?
            //$('#img_'+i).attr('src', 'img/up.gif');
            $log.debug("Chamou Enviando, indice " + i + " total " + total);

            /*$ionicLoading.show({
              template: 'Enviando cupons, aguarde...',
            });*/

            $scope.loading = true;
          }
        );

      return true;
    };
  }

  function removeFile(caminho,tipo,i,total) {

    var db = $window.openDatabase("Database", "1.0", "DoaNotaDB", 1024 * 1024);

    db.transaction(function (tx) {

      /*$log.debug("Entrou para remover o file. Total de Cupons " + total);
       //Criar os registros para suportar os cards
       tx.executeSql('INSERT INTO ULTIMASDOACOES (data, entidade, quantidade, logo, tipo ) VALUES ' +
       '(datetime("now","localtime"),?,?,?)',[total, logo, tipo],historySuccessDB, errorHistoryDB);*/

      if(tipo=='qrcode'){
        var varQuery = 'delete from ENVIANOTA WHERE qrcode = "' + caminho + '"';
      } else {
        var varQuery = 'delete from ENVIANOTA WHERE foto = "' + caminho + '"';
      }

      var qtd = CuponsService.getQtdCupons();
      CuponsService.setQtdCupons(parseInt(qtd)-1);

      validaFimEnvio();

      tx.executeSql(varQuery,[],function (r){
        }
        ,removeSqlNOk);

    }, errorCallback, removeOk());
//	db.transaction(removeImageQuery, erroremove, removeOk);

  };

  function validaFimEnvio(){

    var qtdFim = CuponsService.getQtdCupons();

    $log.debug("Quantidade de Cupons Final é " + qtdFim);

    if(qtdFim==0){

      qtdCuponsTotal=0;

      if( getErros == 0 ){

        alert("Obrigado", "A " + entidade + " agradece a sua ajuda!");
        $ionicLoading.hide();

        //Atualiza a pagina atual para atualizar os cards
        qtdCuponsTotal=0;

      } else {

        alert("Erros", "Faltaram " + getErros + " cupons a serem enviados.");

        /*navigator.notification.alert(
         'Faltaram '+geterros+' cupons',  // message
         null,              // callback to invoke with index of button pressed
         'Envio de Cupons',            // title
         'Voltar'          // buttonLabels
         );*/
      }
    }
  }

  function errorCallback(error){
    $log.error("Erro preparando statement delete de cupons " + error.message);
  }

  function removeOk(){
    $log.debug("Remocao dos cupons com sucesso.");
  }

  return {
    uploadPhoto : uploadPhoto
  }

})

.factory('CardsFactory', function ($q, $http, $ionicPopup, $ionicLoading, $log, UserService, IntegrationData, ApiEndpoint) {

  //funcao para emitir alertas Ionic
  function alert(title, message) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: message,
      cssClass: 'popupDoaNota-buttons'
    });

    alertPopup.then(function (res) {
      $log.debug('Click alert, titulo: ' + title + ', Mensagem: ' + message);
    });
  };

  return {
    getCardData: function (qtdCupons, idCard, index) {

      var q = $q.defer()

      //Carrega os parametros
      var config = {
        params: {
          id_entidade: IntegrationData.getEntityId(),
          email: UserService.getUser().email,
          tipo: "cards",
          hash: "doanota",
          qtd_cupons: qtdCupons,
          id_card: idCard
        }
      };

      $log.debug(config);
      //Armazena a url e config para cards
      var url = ApiEndpoint.url + "/timeline.php?JSON_CALLBACK=?"

      /*var response = {
        data: '({"imagem":"http:\/\/doanota.org\/sistema\/cards\/c1c91c0d5ec2b1481fb63f9883e53ebc.png","mensagem":"Fiz a doa\u00e7\u00e3o de 3 cupons para a Entidade CREAR. Estou em 0\u00ba lugar no ranking mensal com 0 cupons !!!","link":"http:\/\/doanota.org\/androidsp"})'
      };
      q.resolve(response);*/

      $http.get(url, config).then(
        function (response) {

          var returnResponse = {
            quantidade: qtdCupons,
            index: index,
            data: idCard,
            response: response
          }
          q.resolve(returnResponse);

          $log.debug("Resposta do card, idCard: " + idCard + ", response: " + response.data);
          //$ionicLoading.hide();
        },//function(response) Sucesso

        function (response) {
          q.reject("Resposta do DoaNota, idCard " + idCard + " falhou: " + response.data);
          $log.error("Resposta do DoaNota, idCard " + idCard + " falhou: " + response.data);
          //$ionicLoading.hide();
        }//function(response) Error
      )

      return q.promise;

    }//getCardData: function (qtdCupons, idCard) {
  }//return {
})

.factory('LoginFactory', function ($q, $http, $ionicPopup, $ionicLoading, $log, UserService, IntegrationData, ApiEndpoint) {

  return {
    callCardsInit: function (qtdCupons, idCard) {

      var q = $q.defer()

      //Carrega os parametros
      var config = {
        params: {
          id_entidade: IntegrationData.getEntityId(),
          email: UserService.getUser().email,
          tipo: "login",
          hash: "doanota",
          qtd_cupons: qtdCupons,
          id_card: idCard
        }
      };

      //Armazena a url e config para cards
      var url = ApiEndpoint.url + "/json.php?JSON_CALLBACK=?"

      $http.get(url, config).then(
        function (response) {
          q.resolve(response);
          //$ionicLoading.hide();
          $log.debug("LoginFactory, callCardsInit, chamada com sucesso response: " + response.data);
        },//function(response) Sucesso

        function (response) {
          q.reject("Resposta do DoaNota, idCard " + idCard + " falhou: " + response.data);
          $log.error("LoginFactory, callCardsInit, chamada com sucesso response: " + response.data);
        }//function(response) Error
      )

      return q.promise;

    }//callCardsInit: function (qtdCupons, idCard) {
  }//return {
})

.factory('RankingService', function ($q, $http, $ionicPopup, $ionicLoading, $log) {
  //funcao para emitir alertas Ionic
  function alert(title, message) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: message,
      cssClass: 'popupDoaNota-buttons'
    });

    alertPopup.then(function (res) {
      $log.debug('Click alert, titulo: ' + title + ', Mensagem: ' + message);
    });
  };

  return {
    getRankData: function (url, config) {

      $ionicLoading.show({template: 'Carregando...'});

      var q = $q.defer()

      $http.get(url, config).then(
        function (response) {
          q.resolve(response);
          $log.debug("Resposta do DoaNota, ranking com sucesso: " + response.data);

          $ionicLoading.hide();
        },//function(response) Sucesso

        function (response) {
          q.reject("Resposta do DoaNota, ranking falhou: " + response.data);
          $log.error("Resposta do DoaNota, ranking falhou: " + response.data);
          alert('Ranking', 'Ocorreu um erro ao recuperar os dados de Ranking no DoaNota.org. Tente novamente mais tarde.');
          $ionicLoading.hide();
        }//function(response) Error
      )

      return q.promise;

    }//getRankData: function (url, config) {
}//return {

})

.service('DatabaseService', function ($ionicPopup, $log, $window){

  //funcao para emitir alertas Ionic
  function alert(title, message) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: message,
      cssClass: 'popupDoaNota-buttons'
    });

    alertPopup.then(function (res) {
      $log.debug('Click alert, titulo: ' + title + ', Mensagem: ' + message);
    });
  };

  var initialize = function () {
    var db = $window.openDatabase("Database", "1.0", "DoaNotaDB", 200000);
    db.transaction(populateDB, onError, onSuccess);
  }

  function populateDB(tx) {

//	alert('nombre:' + $('#nombredeusuario').val());
//	alert('myname:' + myname);
//	tx.executeSql('DROP TABLE IF EXISTS DOANOTA');
    tx.executeSql('DROP TABLE IF EXISTS ENVIANOTA');
//	tx.executeSql('CREATE TABLE IF NOT EXISTS DOANOTA (id unique, email, multiplas, camera, nome)');
    tx.executeSql('CREATE TABLE IF NOT EXISTS ENVIANOTA (data, foto, status, qrcode)');
    //tx.executeSql('DROP TABLE IF EXISTS ULTIMASDOACOES');
    tx.executeSql('CREATE TABLE IF NOT EXISTS ULTIMASDOACOES (id integer primary key autoincrement, data, entidade, quantidade, logo, tipo)');
  };

  //Callback de erro para populateDB
  function onError(err) {
    alert("Banco de Dados", "Error processing SQL: " + err.message);
  };

  //Callback de sucesso para populateDB
  function onSuccess() {
    $log.debug("Login: Database preparado com sucesso. ");
  };

  return {
    initialize : initialize
  }

})

.service('CuponsService', function ($ionicPopup, $window, $log, $q){

  //funcao para emitir alertas Ionic
  function alert(title, message) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: message,
      cssClass: 'popupDoaNota-buttons'
    });

    alertPopup.then(function (res) {
      $log.debug('Click alert, titulo: ' + title + ', Mensagem: ' + message);
    });
  };


  // For the purpose of this example I will store user data on ionic local storage but you should save it on a database
  var setQtdCupons = function(qtdCupons) {
      window.localStorage.setItem("localqtdcoupons", qtdCupons);
  };

  var getLocalQtdCupons = function() {
    return parseInt(window.localStorage.getItem("localqtdcoupons"));
  }


  var getQtdCupons = function(){
    //return parseInt(window.localStorage.getItem("localqtdcoupons"));

    var q = $q.defer();

    var qtdBanco = 0;

    var db = $window.openDatabase("Database", "1.0", "DoaNotaDB", 200000);
    db.transaction(  //Query para a quantidade de cupons
      function (tx) {
        $log.debug("Cupons Service, select quantidade de cupons.");
        tx.executeSql('SELECT data FROM ENVIANOTA', [],   function (tx, results) {

          qtdBanco = results.rows.length;
          $log.debug("Recuperou a quantidade cupons. Valor: " + qtdBanco);

          q.resolve(qtdBanco);

        }, erroExecuteSql);
      }
      , erroDbTransaction);

    return q.promise;;
  };

  var clearCuponsQtd = function() {
    window.localStorage.clear("localqtdcoupons");
  }

  function erroExecuteSql(err) {
    //alert("Erro", "Erro recuperando a quantidade cupons. Reinicie a aplicação. " + err.message);
    $log.debug("Erro recuperando a quantidade cupons. Reinicie a aplicação. " + err.message)
  }

  function erroDbTransaction(err) {
    //alert("Erro", "Erro ao verificar a quantidade de cupons. Verifique a fila de cupons do menu: " + err.message);
    $log.debug("Erro ao verificar a quantidade de cupons. Verifique a fila de cupons do menu: " + err.message)

  }

  return {
    getQtdCupons: getQtdCupons,
    setQtdCupons: setQtdCupons,
    clearCuponsQtd: clearCuponsQtd
  };
})


.factory('jQueryLikeSerializeFixed', function() {

  /**
   * This method is intended for encoding *key* or *value* parts of query component. We need a custom
   * method because encodeURIComponent is too aggressive and encodes stuff that doesn't have to be
   * encoded per http://tools.ietf.org/html/rfc3986:
   *    query       = *( pchar / "/" / "?" )
   *    pchar         = unreserved / pct-encoded / sub-delims / ":" / "@"
   *    unreserved    = ALPHA / DIGIT / "-" / "." / "_" / "~"
   *    pct-encoded   = "%" HEXDIG HEXDIG
   *    sub-delims    = "!" / "$" / "&" / "'" / "(" / ")"
   *                     / "*" / "+" / "," / ";" / "="
   */
  function encodeUriQuery(val, pctEncodeSpaces) {
    return encodeURIComponent(val).
    replace(/%40/gi, '@').
    replace(/%3A/gi, ':').
    replace(/%24/g, '$').
    replace(/%2C/gi, ',').
    replace(/%3B/gi, ';').
    replace(/%20/g, (pctEncodeSpaces ? '%20' : '+'));
  }

  function serializeValue(v) {
    if (angular.isObject(v)) {
      return angular.isDate(v) ? v.toISOString() : angular.toJson(v);
    }
    return v;
  }

  function forEachSorted(obj, iterator, context) {
    var keys = Object.keys(obj).sort();
    for (var i = 0; i < keys.length; i++) {
      iterator.call(context, obj[keys[i]], keys[i]);
    }
    return keys;
  }

  /**
   * Fixed version of $httpParamSerializerJQLike
   * $httpParamSerializerJQLike with the current version of Angular for Ionic
   * does not serialize array indices correctly. This updated version was pulled from here
   * https://github.com/ggershoni/angular.js/blob/0c98ba4105d50afc1fde3f7a308eb13d234d0e57/src/ng/http.js
   * @param  {[Object]} params
   * @return {[String]} Serialized data
   */
  function jQueryLikeParamSerializer(params) {
    if (!params) return '';
    var parts = [];
    serialize(params, '', true);
    return parts.join('&');

    function serialize(toSerialize, prefix, topLevel) {
      if (toSerialize === null || angular.isUndefined(toSerialize)) return;
      if (angular.isArray(toSerialize)) {
        angular.forEach(toSerialize, function(value, index) {
          serialize(value, prefix + '[' + (angular.isObject(value) ? index : '') + ']');
        });
      } else if (angular.isObject(toSerialize) && !angular.isDate(toSerialize)) {
        forEachSorted(toSerialize, function(value, key) {
          serialize(value, prefix +
            (topLevel ? '' : '[') +
            key +
            (topLevel ? '' : ']'));
        });
      } else {
        parts.push(encodeUriQuery(prefix) + '=' + encodeUriQuery(serializeValue(toSerialize)));
      }
    }
  };
  return jQueryLikeParamSerializer;
})

.factory('ConnectivityMonitor', function($rootScope, $cordovaNetwork){

  return {
    isOnline: function(){
      if(ionic.Platform.isWebView()){
        return $cordovaNetwork.isOnline();
      } else {
        return navigator.onLine;
      }
    },
    isOffline: function(){
      if(ionic.Platform.isWebView()){
        return !$cordovaNetwork.isOnline();
      } else {
        return !navigator.onLine;
      }
    },
    startWatching: function(){
      if(ionic.Platform.isWebView()){

        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
          console.log("went online");
        });

        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
          console.log("went offline");
        });

      }
      else {

        window.addEventListener("online", function(e) {
          console.log("went online");
        }, false);

        window.addEventListener("offline", function(e) {
          console.log("went offline");
        }, false);
      }
    }
  }
})

.factory('IntegrationData', function($rootScope, ApiEndpoint){

  var entityId = 56;
  var appVersion = "2.2.0";
  
  return {
    getType: function () {
      return "entidade";
    },

    getOrigin: function () {
      return entityId;
    },

    getEntityId: function () {
      return entityId;
    },

    getAppName: function () {
      return "AMCIP";
    },

    getEntity: function () {
      return "AMCIP";
    },

    getLogo: function () {
      return ApiEndpoint.url + "/imagens/entidades/00960645000176_grande.png";
    },

    getAndroidShareUrl: function () {
      return "http://doanota.org/app/?platform=android&id=" + entityId;
    },

    getAppleShareUrl: function () {
      return "http://doanota.org/app/?platform=apple&id=" + entityId;
    },

    getAppVersion: function () {
      return appVersion;
    },
  }
})
;
