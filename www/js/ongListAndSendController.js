angular.module('OngCtrl', [])

.controller('OngCtrl', function($rootScope, $scope, $state, $http, UserService, $ionicLoading, $ionicPopup, $ionicHistory, CuponsService, $window,
                                LocationService, $cordovaFileTransfer, IntegrationData, jQueryLikeSerializeFixed, ApiEndpoint, $log, $q) {

  $log.debug('Entrou no controlador Ong');

  getLocation();

  var qtdCuponsTotal = 0;
  var varImageURI;
  var enviarListar   = 2;
  var entidade       = "";
  var logo           = "";
  var totalCupons    = 0;


  var idEntidade     = 0;
  var getErros       = 0;
  var origem         = IntegrationData.getOrigin(); //1000 Sao Paulo, 1001 Parana
  var tipoLista      = IntegrationData.getType(); //entidade Sao Paulo, entidadepr Parana, entidadeCrianca Crianca

  $log.debug("Carregou Origem: " + origem + " e tipo: " + tipoLista);


  var latitude ;
  var longitude;

  function getLocation() {
    //Recupera a localidade
    LocationService.getLocation().then(

      function(res) {
        $rootScope.latitude  = res.latitude;
        $rootScope.longitude = res.longitude;
        $log.debug("Valores recuperados de Location " + $rootScope.latitude + " " + $rootScope.longitude);
      },
      function(err) {
        $log.error("Erro ao recuperar a localidade: " + err.message);
      }
    )
  };

  function getQtdCupons() {

    var q = $q.defer();

    var qtd = 0;
    //Chama o Service que recupera a quantidade do armazenamento local.
    CuponsService.getQtdCupons().then(

      function(result) {
        $log.debug("recebido retorno, quantidade: " + result);
        qtd = result;


        q.resolve(qtd);

        return q.promise;
      });
  }


  //funcao para emitir alertas Ionic
  function alert(title, message) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: message,
      cssClass: 'popupDoaNota-buttons'
    });
  };

  $scope.$on('$ionicView.beforeEnter', function(e) {
    //Atualiza a lista de entidades a cada visita à pagina.
    reloadOngList();
  });

  function reloadOngList() {

    //Recebe o array direto da pagina de doa nota.
    //$scope.ongs = $state.params.obj;
    //var len = $state.params.obj.length;
    //$log.error("tamanho do array recebido é " + len);

    $ionicLoading.show({template: "<ion-spinner></ion-spinner> <br/> Carregando Entidades... "});

    //Armazena a url de entidades
    var url = ApiEndpoint.url + "/json.php?JSON_CALLBACK=?";
    //Carrega os parametroasf
    var config = {
      params: {
        hash: "doanota"
       ,tipo: tipoLista}
    };

    $http.get(url, config).then(
      function(response) {
        $log.error("Resposta da lista de entidades com sucesso. " + response.data);

        //Extrai a resposta do Json.
        var jsonResponse = $scope.$eval(response.data); //angular.fromJson(jsonData[1]);*/

        //var response = '([{"id_entidade":"17","razao":"CERVIADO - Centro de Reestrutura\u00e7\u00e3o para a Vida","cnpj":"03.806.878\/0001-07","causa":"Assist. mulher,v\u00edtima de viol\u00eancia"},{"id_entidade":"12","razao":"Instituto Adhara","cnpj":"11.512.700\/0001-81","causa":"Inclu. Social pesso. com surdez"},{"id_entidade":"18","razao":"Instituto Cisne","cnpj":"56.322.696\/0001-27","causa":"Organiza\u00e7\u00e3o Social de Sa\u00fade"},{"id_entidade":"19","razao":"VILA VICENTINA BAURU","cnpj":"45.023.371\/0001-27","causa":"Abrigo para Idosos"},{"id_entidade":"15","razao":"Projeto AMMA","cnpj":"17.991.164\/0001-20","causa":"Crian\u00e7as e Adolescetes"},{"id_entidade":"3","razao":"Uniao Jd Colombo","cnpj":"58.933.391\/0001-87","causa":"Assist\u00eancia Social"},{"id_entidade":"6","razao":"Organiza\u00e7\u00e3o GABRIEL","cnpj":"04.333.593\/0001-69","causa":"Sa\u00fade"},{"id_entidade":"14","razao":"Gotas de Flor com Amor","cnpj":"71.740.732\/0001-66","causa":"Crian\u00e7as e Jovens"},{"id_entidade":"11","razao":"Instituto Relfe Peru\u00edbe","cnpj":"12.947.128\/0001-46","causa":"Crian\u00e7as e Adolescentes "},{"id_entidade":"2","razao":"Boror\u00e9","cnpj":"59.936.781\/0001-73","causa":"Crian\u00e7as Carentes"},{"id_entidade":"16","razao":"Associa\u00e7\u00e3o Aquarela","cnpj":"08.562.948\/0001-42","causa":"Crian\u00e7as e Adolescetes"},{"id_entidade":"9","razao":"Casas Andr\u00e9 Luiz","cnpj":"62.220.637\/0001-40","causa":"Atend. ao Deficiente Intelectual"},{"id_entidade":"5","razao":"Institui\u00e7\u00e3o Attittude","cnpj":"16.589.579\/0001-06","causa":"Artes e Educa\u00e7\u00e3o"},{"id_entidade":"4","razao":"Projeto JEDA","cnpj":"55.032.338\/0001-17","causa":"Inclus\u00e3o Social"},{"id_entidade":"10","razao":"Nucleo Espiral","cnpj":"10.298.541\/0001-00","causa":"Prev. viol. contra crian\u00e7as e adolescentes"},{"id_entidade":"7","razao":"Projeto CREAR","cnpj":"00.956.660\/0001-40","causa":"Crian\u00e7as Carentes"},{"id_entidade":"8","razao":"COFASP S\u00e3o Pedro","cnpj":"45.113.750\/0001-08","causa":"Crian\u00e7as e adolescentes Carentes"}]);';
        //var jsonResponse = $scope.$eval(response); //angular.fromJson(jsonData[1]);*/

        $ionicLoading.hide();

        //Carrega a lista
        $scope.ongs = [];

        //Varre o array do json e carrega o array da tela
        var i = 0;
        jsonResponse.forEach(function(obj) {

          var cnpj_num = obj.cnpj.replace(/\./g,'').replace(/\//g,'').replace(/\-/g,'');

          $scope.ongs[i] = {
            id_entidade: obj.id_entidade,
            cnpj: obj.cnpj,
            cnpj_num: cnpj_num,
            razao: obj.razao,
            logo: "http://doanota.org/sistema/imagens/entidades/" + cnpj_num + "_menu.png",
            causa: obj.causa
          };
          i = i + 1;
        });

        var len = $scope.ongs.length;
        $log.debug('Tamanho do array ' + len);

        //Atualiza a view doaNota
        $rootScope.doRefresh();

      },//Function Callback sucesso

      function(error) {
        $ionicLoading.hide();
        $log.error("Erro na resposta da lista de entidades do DoaNota " + error.data);
        alert("Erro", "Erro ao listar as Entidades Parceiras do DoaNota. Tente novamente mais tarde.");

        $state.go('app.doaNota');
      });//function callback Error
  }


  $rootScope.marcaEntidade = function(entidade, idEntidade, logo) {
    $scope.marcaEntidade(entidade, idEntidade, logo);
  }

  $rootScope.$on("marcaEntidade", function (event, args) {
    $scope.marcaEntidade(args.entidade, args.idEntidade, args.logo);

    $scope.$on('$destroy', function () {
      $log.debug("Não escuta mais marcaentidade.");

    });

  });

    $scope.marcaEntidade = function (varEntidade, varIdEntidade, varLogo){
      entidade = varEntidade;
      logo     = varLogo;

    idEntidade = varIdEntidade;

    var qtdCupons = 0;
    //Chama o Service que recupera a quantidade do armazenamento local.
    CuponsService.getQtdCupons().then(

      function(result) {
        qtdCupons =  result;

        var confirmDonation = $ionicPopup.confirm({
          title: "Envio de Cupons",
          template: 'Confirma doação de ' + qtdCupons  + ' Cupom(s)',
          cssClass: 'popupDoaNota-buttons',
          cancelText: 'Listar',
          okText: 'Enviar'
        });

        confirmDonation.then(function(res) {
          if(res) {
            $log.debug('Confirmado envio de cupons.');
            onConfirmEntidade();
          } else {
            $log.debug('Nao confirmado, clicado em listar.');
            onListarCupons(entidade);
          }
        });

      })
  };//function marcaEntidade(varEntidade){

  function onConfirmEntidade() {
    enviarListar = 1;
    $log.debug("Envio de Cupons, chama enviarCuponsUpload(). O id da entidade é " + idEntidade);
    enviarCuponsUpload(1);
  }

  function onListarCupons() {
    //alert("Listar, Implementar aqui a funcao de listar.");
    enviarListar = 2;
    enviarCuponsUpload();
    //$state.go('app.sendQueue', {entidade: entidade});

  }

  function enviarCuponsUpload(){
    var db = $window.openDatabase("Database", "1.0", "DoaNotaDB", 200000);
    db.transaction(queryDB, errorDB);
  }

  function queryDB(tx) {
    qtdCuponsTotal = getQtdCupons();
    tx.executeSql("SELECT strftime('%d-%m-%Y %H:%M:%S',data) as data,foto,qrcode FROM ENVIANOTA ", [], querySuccessLista, errorLista);
    //tx.executeSql("SELECT 1 FROM ENVIANOTA", [], querySuccessLista, errorLista);
  }

  function errorDB(err) {
    alert("Erro SQL ao recuperar as notas: " + err.message );
  };

  function querySuccessLista(tx, results) {

    $log.debug("Query com sucesso, depois do Select notas, quantidade: " + results.rows.length);

     //$('#enviando').append("<br>>querySuccessLista>> depois select notas ..");
     //$.mobile.changePage("#enviando_lista");
     //document.getElementById('btncupons').style.display = "none";
     var total = results.rows.length;

     $log.debug("Tamanho do Banco: " + total);

     if(total >= 1){

       $scope.queue = [];

     for(var i=0;i<total;i++) {

       varimageURI = results.rows.item(i).foto;

       $scope.queue[i] = {
         index: i,
         foto: results.rows.item(i).foto,
         data: results.rows.item(i).data,
         qrcode: results.rows.item(i).qrcode
       }

       //filaProcessamento(i,results.rows.item(i).foto,results.rows.item(i).data,results.rows.item(i).qrcode)
     }

     if (enviarListar == 2) {
       $log.debug("Listar entidades.");
       $state.go('app.sendQueue',  {entidade: entidade, idEntidade: idEntidade, logo: logo, obj: $scope.queue});
     }

     if(enviarListar==1){
     //$.mobile.showPageLoadingMsg("b", "Faltam  " + getqtdCupons() + " de " + qtdCuponsTotal, false);
       for(var i=0;i<total;i++) {
         $log.debug("Fazer Upload");
         uploadPhoto(results.rows.item(i).foto,results.rows.item(i).qrcode,i,total,entidade,idEntidade);
       }

     }
     } else {
       //filaProcessamento(-1,0,0,0);
       alert("Sem Cupons", "Não há cupons à serem exibidos.");
     }
  };

  function errorLista(err) {
    alert("Erro", "Erro ao recuperar a lista de cupons: " + err.message)
  };

  function uploadPhoto(imageURI,qrcode,i,total,entidade,idEntidade) {

    var params = {};

    //Parametros do dispositivo
    var deviceInformation = ionic.Platform.device();
    params.uuid           = deviceInformation.uuid;
    params.name           = deviceInformation.model;
    params.version        = deviceInformation.version;

    //Dados da plataforma
    params.platform        = ionic.Platform.platform();

    //Dados da Tela
    params.screenwidth     = $window.innerWidth;
    params.screenheight    = $window.innerHeight;

    //Usuario
    params.email           = UserService.getUser().email;

    //Localidade
    params.lat             = latitude;
    params.long            = longitude;

    //Parametros
    params.caminho         = imageURI;
    params.id_entidade     = idEntidade;
    params.qrcode          = qrcode;
    params.origem          = origem;

    var totalCupons = CuponsService.getQtdCupons();

    $log.debug("Preparando envio, id da entidade é: " + idEntidade);

    if (imageURI == "") {
      $log.debug("Image nao encontrada. Rotina de envio QRCode.");

      //Implementar GIF ?
      $log.debug("Chamou Enviando QR, indice " + i + " total " + totalCupons);

      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner> <span>  Enviando ' + totalCupons + ' cupons...  </span>',
        duration: 30000
      }

      );

      //Carrega a variavel de configuracao
      var config = {
        parametros: params
      };

      $log.debug("Variável config: " + JSON.stringify(config));

      //Carrega a url e concatena com a lista de parametros preparadas como jquery.
      var url = "http://doanota.org/sistema/receive_qrcode.php?JSON_CALLBACK=?" + jQueryLikeSerializeFixed(config) ;

      $http.get(url).then(
        function (response) {

          $log.debug("Resposta com sucesso: " + response.data);

            var jsonResponse = $scope.$eval(response.data);//angular.fromJson(jsonData[1]);

          if (i == total -1 ) {
            $ionicLoading.hide();
          };

          //Valida a resposta do servidor
          if (jsonResponse.validacion == "") {

            alert("Erro no Envio", "Erro ao enviar os dados QRCode ao DoaNota. Tente novamente.");
            $state.go("app.doaNota");

          } else {

            $log.debug("Envio dos dados de QRCode com sucesso. Resposta é: " + response.data);

            removeFile(qrcode,'qrcode',i,total);
1
          }

        },//Success Callback

        function (response) {

          $log.error("Erro ao enviar os dados de QRCode ao DoaNota(GET). Status: " + response.status + ", response: " + response.data);

          if (i == total -1 ) {
            $ionicLoading.hide();
          };
          //Acumula a Variável de erros
          getErros = getErros + 1;

        }//function (error) {
      )


    } else {
      $log.debug("ImageURI encontrada. Rotina de envio Imagem.");

      varImageURI = imageURI;

      var options = new FileUploadOptions();

      options.eKey = "file";
      options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
      options.mimeType = "image/jpeg";
      options.params = params;
      options.chunkedMode = false;

      $cordovaFileTransfer.upload("http://doanota.org/img/upload.php", imageURI, options)
        .then(
          function (result) {
            $log.debug("Sucesso ao enviar a imagem, i=" + i + " total=" + total);

            if (i == total - 1) {
              $ionicLoading.hide();
            }

            removeFile(params.caminho,'foto',i,total);

          },
          function (err) {
            $log.error("Erro enviando a imagem." + err.message);

            //Acumula a variável de erros
            getErros = getErros + 1;
          },
          function (progress) {
            $log.debug("Enviando a imagem.");

            //Implementar GIF?
            //$('#img_'+i).attr('src', 'img/up.gif');
            $log.debug("Chamou Enviando, indice " + i + " total " + total);
            $ionicLoading.show({
              template: 'Enviando cupons, aguarde...',
              duration: 30000
            });
          }
        );

      return true;
    };
  }

  function removeFile(caminho,tipo,i,total) {

    var db = $window.openDatabase("Database", "1.0", "DoaNotaDB", 1024 * 1024);

    db.transaction(function (tx) {

       //Criar os registros para suportar os cards
      tx.executeSql('INSERT INTO ULTIMASDOACOES (data, entidade, logo, tipo ) VALUES ' +
        '(datetime("now","localtime"),?,?,?)',[entidade, logo, tipo],historySuccessDB, errorHistoryDB);

      if(tipo=='qrcode'){
        var varQuery = 'delete from ENVIANOTA WHERE qrcode = "' + caminho + '"';
      } else {
        var varQuery = 'delete from ENVIANOTA WHERE foto = "' + caminho + '"';
      }

      /*var qtd = CuponsService.get;
      CuponsService.setQtdCupons(parseInt(qtd)-1);*/

      tx.executeSql(varQuery,[],function (r){
        $log.debug("Remocao dos cupons com sucesso. VALOR do total de CUPONS: " + i + " total geral: " + totalCupons);

        totalCupons = totalCupons -1;

        }
        ,removeSqlNOk);

      if (totalCupons == 1) {
        validaFimEnvio(totalCupons);
      }

    }, errorCallback, removeOk());
    //	db.transaction(removeImageQuery, erroremove, removeOk);

  };

  function validaFimEnvio(qtd){

    //Chama o Service que recupera a quantidade do armazenamento local.
    $log.debug("SendQueue Quantidade de Cupons Final recebida é " + qtd);

    if(qtd==1){

      qtdCuponsTotal=0;

      if( getErros == 0 ){

        //Desativa a navegacao da proxima tela.
        $ionicHistory.nextViewOptions({
          disableBack: true
        });

        $state.go("app.doaNota");

        alert("Obrigado", "A " + entidade + " agradece a sua ajuda!");
        qtdCuponsTotal=0;

      } else {

        alert("Erros", "Faltaram " + getErros + " cupons a serem enviados.");

      }
    }
  };


  function errorCallback(error){
    $log.error("Erro preparando statement delete de cupons " + error.message);
  }

  function removeSqlNOk(error){
    $log.debug("Erro executando delete de cupons " + error.message);
  }

  function removeOk(){

  }

  function historySuccessDB() {
    $log.debug("Inseriu o historico na tabela ULTIMASDOACOES.");
  }

  function errorHistoryDB(err) {
    $log.error("Não inseriu o historico na tabela Ultimas Doaçoes. Verifique " + err.message)
  };


  function errorCallback(error){
    $log.error("Erro preparando statement delete de cupons " + error.message);

  }

});

