/**
 * Created by cristiano on 27/07/16.
 */
angular.module('RankingCtrl', [])

.controller('RankingCtrl', function ($scope, $http, UserService, RankingService, $ionicPopup, $log, IntegrationData, ApiEndpoint) {

  var origem = IntegrationData.getOrigin();
  $log.debug("Carregou Origem: " + origem);

  //funcao para emitir alertas Ionic
  function alert(title, message) {
    var alertPopup = $ionicPopup.alert({
      title: title,
      template: message,
      cssClass: 'popupDoaNota-buttons'
    });

    alertPopup.then(function (res) {
      $log.debug('Click alert, titulo: ' + title + ', Mensagem: ' + message);
    });
  };

  $log.debug("Entrou no controlador do Ranking.");

  //Recupera o email do usuario
  var email = UserService.getUser().email;
  var hash  = "doanota";
  var tipo  = "ranking";

  $scope.ranking = {};

  //Monta as configuracoes para chamada
  var config = {
    params: {
      usuario: email,
      hash: hash,
      tipo: tipo,
      origem: origem
    }
  };

  //Armazena a URL
  var url = ApiEndpoint.url + "/json.php?JSON_CALLBACK=?";

  //Define as variaves de placares
  var placarGeral = [];
  var placarMensal = [];
  var placarSemanal = [];


  //Efetua a chamada para recuperar o ranking
  var promise = RankingService.getRankData(url,config);

  //Funcao que aguarda a promessa. Somente aqui as variaves tem valor, fora se tornam assincronas
  promise.then(function(result){

    var jsonResponse = $scope.$eval(result.data);//angular.fromJson(jsonData[1]);

    $log.debug("Resposta doaNota: " + jsonResponse);

    //Carrega as variaveis de promessa
    placarGeral = jsonResponse.geral.split("|");
    placarMensal = jsonResponse.mensal.split("|");
    placarSemanal = jsonResponse.semanal.split("|");

    if(placarGeral[0]==0){
      $scope.ranking.geral = "Você não possui Placar";
    } else {
      $scope.ranking.geral = "Você é o " + placarGeral[0] + " colocado com " + placarGeral[1] + " cupom(s)";
    }

    if(placarMensal[0]==0){
      $scope.ranking.mensal = "Você não possui Placar";
    } else {
      $scope.ranking.mensal = "Você é o " + placarMensal[0] + " colocado com " + placarMensal[1] + " cupom(s)";
    }

    if(placarSemanal[0]==0){
      $scope.ranking.semanal = "Você não possui Placar";
    } else {
      $scope.ranking.semanal = "Você é o " + placarSemanal[0] + " colocado com " + placarSemanal[1] + " cupom(s)";
    }

    if(placarGeral[0]==1){
      $scope.ranking.geralFalta = "Parabéns !!!";
    } else {
      $scope.ranking.geralFalta = "Faltam " + placarGeral[2] + " cupom(s) para você ser o primeiro";
    }

    if(placarMensal[0]==1){
      $scope.ranking.mensalFalta = "Parabéns pela conquista";
    } else {
      $scope.ranking.mensalFalta = "Faltam " + placarMensal[2] + " cupom(s) para você ser o primeiro";
    }

    if(placarSemanal[0]==1){
      $scope.ranking.semanalFalta ="Parabéns pela conquista";
    } else {
      $scope.ranking.semanalFalta = "Faltam " + placarSemanal[2] + " cupom(s) para você ser o primeiro";
    }

  });//promise.then(function(result){



});
